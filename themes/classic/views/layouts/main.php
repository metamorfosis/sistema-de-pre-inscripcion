<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->pageTitle;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="UNEFA">
    <meta name="author" content="Equipo Número 5">

    <!-- Styles -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/public/classic/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/public/classic/css/responsive.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/public/classic/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/public/classic/css/estilo.css" rel="stylesheet">
    <?php Yii::app()->bootstrap->register(); ?>
    <?php Yii::app()->yiistrap->register(); ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons
    --><link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/public/classic/ico/logo.png">
  </head>
  <body>

  <div class="navbar navbar-static-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#">S.A.R.C.H</a>

        <div class="nav-collapse collapse pull-right">

          <?php
            $admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
            $coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
            $profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
            $visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
            $visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
            $visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

            $this->widget(
              'yiistrap.widgets.TbNav',
              array(
                'type' => TbHtml::NAV_TYPE_PILLS,
                'items' => array(
                  array('label'=>Yii::t('app', 'Home'), 'url'=>array('/site/index')),
                  array('label'=>Yii::t('app', 'Account'), 'url'=>array('/users/account', 'id'=>Yii::app()->user->id),'visible' => $visible3),
                  array('label'=>Yii::t('app', 'Administer'),
                    'items' => array(
                      array('label' => Yii::t('app', 'Users'), 'url' => array('/users/index'),'visible' => $visible),
                      array('label' => Yii::t('app', 'Career'), 'url' => array('/careers/index'), 'visible' => $visible),
                      array('label' => Yii::t('app', 'Pensum'), 'url' => array('/pensums/admin'), 'visible' => $visible),
                      array('label' => Yii::t('app', 'Semesters'), 'url' => array('/semesters/index'), 'visible' => $visible),
                      array('label' => Yii::t('app', 'Matter'), 'url' => array('/matters/index'), 'visible' => $visible),
                      array('label' => Yii::t('app', 'Classroom'), 'url' => array('/classrooms/index'), 'visible' => $visible),
                      array('label'=>Yii::t('app','Schedules'), 'url'=>array('/schedules/index'), 'visible'=>$visible),
//                      array('label' => Yii::t('app', 'Studentslists'), 'url' => array('/studentslist/index'), 'visible' => $visible),
                    ),
                  ),
                  array('label'=>Yii::t('app', 'Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                  array('label'=>Yii::t('app', 'Welcome').' ( '.Yii::app()->user->name.' )', 'url'=>array('/site/index'), 'active' => false, 'visible'=>!Yii::app()->user->isGuest, 'items' => array(
                    TbHtml::menuDivider(),
                    array('label' => Yii::t('app', 'Logout'), 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                  ),
                ),
              )
            );
          ?>

        </div>  <!--/.nav-collapse -->

      </div>
    </div>
  </div>

  <?php
    if(isset($this->breadcrumbs) and $this->breadcrumbs !== array())
    {
    	echo '
          <div class="container">
            <div class="row-fluid">
              <div class="span12">
                ';
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'htmlOptions'=>array("style"=>"margin: 10px 0;"),
                    'links'=>$this->breadcrumbs,
                  ));
                echo'
              </div>
            </div>
          </div>'; //Fin Breadcrumbs
    }
    else
    {
    	//var_dump(isset($this->breadcrumbs) and $this->breadcrumbs !== array()); die();
    	//echo "string";
    }
  ?>
  <?php
    $flashMessages = Yii::app()->user->getFlashes();
    if($flashMessages){
      echo '
        <div class="container">
          <div class="row-fluid">
            <div class="span12">
              <div class="info" style="text-align: left;">
                <ul class="flashes">';
                foreach ($flashMessages as $key => $messages) {
                  echo '
                  <li>
                    <div class="flash-'.$key.'">'.$messages.'</div>
                  </li>';
                }
      echo'
                </ul>
              </div>
            </div>
          </div>
        </div>';
    }
  ?>
  </div>

  <?php echo $content; ?> <!-- Contenido -->

  <br>
  <footer class="footer-credits">
    <div class="container">
        <ul class="clearfix">
            <li>©.i. 2017. Todos los Derechos Reservados.</li>
            <li>Desarrollado por: Equipo Número 5 </li>
        </ul>
    </div>
    <!--close footer-credits container-->
  </footer>

  <script src="<?php echo Yii::app()->baseUrl; ?>/public/classic/js/jquery.js"></script>
  <script src="<?php echo Yii::app()->baseUrl; ?>/public/classic/js/jquery.ba-bbq.min.js"></script>
  <script src="<?php echo Yii::app()->baseUrl; ?>/public/classic/js/bootstrap.min.js"></script>
  <script src="<?php echo Yii::app()->baseUrl; ?>/public/classic/js/jquery.rating.pack.js"></script>
  <?php
    Yii::app()->clientScript->registerScript(
      'myHidenEffect',
      '$(".info").animate({opacity: 1.0}, 5000).slideUp("slow");',
      CClientScript::POS_READY
      );
  ?>
  </body>
</html>
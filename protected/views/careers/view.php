<?php
/* @var $this CareersController */
/* @var $model Careers */

$this->breadcrumbs=array(
    Yii::t('app','Careers')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('app','List Careers'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Careers'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Careers'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Careers'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View Careers').' #'.$model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>

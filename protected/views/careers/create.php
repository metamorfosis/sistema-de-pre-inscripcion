<?php
/* @var $this CareersController */
/* @var $model Careers */

$this->breadcrumbs=array(
    Yii::t('app','Careers')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Careers'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Careers'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Careers'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
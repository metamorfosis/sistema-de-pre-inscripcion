<?php
/* @var $this CareersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Careers'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Careers'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Careers'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Careers'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

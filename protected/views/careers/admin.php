<?php
/* @var $this CareersController */
/* @var $model Careers */

$this->breadcrumbs=array(
    Yii::t('app','Careers')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Careers'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Careers'), 'url'=>array('create')),
);
?>

<h1><?php echo Yii::t('app','Manage Careers'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'careers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'description',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

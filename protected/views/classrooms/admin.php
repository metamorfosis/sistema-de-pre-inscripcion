<?php
/* @var $this ClassroomsController */
/* @var $model Classrooms */

$this->breadcrumbs=array(
    Yii::t('app','Classrooms')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Classrooms'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Classrooms'), 'url'=>array('create')),
);

?>

<h1><?php echo Yii::t('app','Manage Classrooms'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'classrooms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cod',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this ClassroomsController */
/* @var $model Classrooms */

$this->breadcrumbs=array(
    Yii::t('app','Classrooms')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Classrooms'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Classrooms'), 'url'=>array('create')),
	array('label'=>Yii::t('app','View Classrooms'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Classrooms'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Classrooms').' '.$model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
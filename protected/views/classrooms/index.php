<?php
/* @var $this ClassroomsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Classrooms'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Classrooms'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Classrooms'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Classrooms'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this ClassroomsController */
/* @var $model Classrooms */

$this->breadcrumbs=array(
    Yii::t('app','Classrooms')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List Classrooms'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Classrooms'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Classrooms'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete Classrooms'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Classrooms'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View Classrooms').' #'.$model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cod',
	),
)); ?>

<?php
/* @var $this ClassroomsController */
/* @var $model Classrooms */

$this->breadcrumbs=array(
    Yii::t('app','Classrooms')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Classrooms'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Classrooms'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Classrooms'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
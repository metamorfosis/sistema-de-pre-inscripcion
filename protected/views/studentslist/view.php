<?php
/* @var $this StudentslistController */
/* @var $model Studentslist */

$this->breadcrumbs=array(
    Yii::t('app','Studentslists')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List Studentslist'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Studentslist'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Studentslist'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete Studentslist'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Studentslist'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View Studentslist').' #'.$model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'classroom_id',
	),
)); ?>

<?php
/* @var $this StudentslistController */
/* @var $model Studentslist */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div>
		<?php echo $form->textFieldRow($model,'id'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'user_id'); ?>
	</div>

	<div>
        <?php echo $form->textFieldRow($model,'classroom_id'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton(Yii::t('app','Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
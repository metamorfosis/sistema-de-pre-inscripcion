<?php
/* @var $this StudentslistController */
/* @var $model Studentslist */

$this->breadcrumbs=array(
    Yii::t('app','Studentslists')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Studentslist'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Studentslist'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Studentslist') ?></h1>

<?php $this->renderPartial('_list', array('model'=>$model)); ?>
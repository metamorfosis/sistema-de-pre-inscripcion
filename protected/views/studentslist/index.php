<?php
/* @var $this StudentslistController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Studentslists'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Studentslist'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Studentslist'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Studentslists') ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

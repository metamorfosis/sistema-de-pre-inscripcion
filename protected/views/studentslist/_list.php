<?php
/* @var $this StudentslistController */
/* @var $model ListForm */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'list-form',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <p class="note"><?php echo Yii::t('app', 'Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->textField($model,'userNumber'); ?>
		<?php echo $form->error($model,'userNumber'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton(Yii::t('app','Create'), array("class"=>"btn btn-primary btn-lg")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
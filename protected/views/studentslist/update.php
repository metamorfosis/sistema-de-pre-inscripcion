<?php
/* @var $this StudentslistController */
/* @var $model Studentslist */

$this->breadcrumbs=array(
    Yii::t('app','Studentslists')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
    array('label'=>Yii::t('app','List Studentslist'), 'url'=>array('index')),
    array('label'=>Yii::t('app','Create Studentslist'), 'url'=>array('create')),
    array('label'=>Yii::t('app','View Studentslist'), 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>Yii::t('app','Manage Studentslist'), 'url'=>array('admin')),
    );
?>

    <h1><?php echo Yii::t('app','Update Studentslist').' #'.$model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
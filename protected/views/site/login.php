<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php
				$this->pageTitle=Yii::app()->name . ' - ' . Yii::t('app','Login');
				$this->breadcrumbs=array(
					Yii::t('app','Login'),
					);
			?>

			<h1><i class="fa fa-sign-in fa-fw"></i> <?php echo Yii::t('app','Login') ?></h1>

			<p><?php echo Yii::t('app','Please fill out the following form with your login credentials:') ?></p>
			<div class="form">
				<?php
					$form=$this->beginWidget('yiistrap.widgets.TbActiveForm',
						array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
						)
					);
				?>

				<p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.') ?></p>
				<br>
				<div>
					<?php echo $form->textFieldControlGroup($model,'username',
						array('placeholder' => 'Introduzca su Nombre de Usuario.')); ?>
				</div>
				<div>
					<?php echo $form->passwordFieldControlGroup($model,'password',
						array('placeholder' => 'Introduzca su Contraseña.')); ?>
				</div>
				<div>
					<?php echo $form->checkBoxControlGroup($model, 'rememberMe',
						array('Marque si desea ser recordado')); ?>
				</div>
				<br>

				<div class="buttons">
					<?php echo CHtml::submitButton(Yii::t('app','Login'), array("class"=>"btn btn-primary btn-lg")); ?>
				</div>

				<?php $this->endWidget(); ?>
				<br>
			</div><!-- form -->
		</div>
	</div>
</div>


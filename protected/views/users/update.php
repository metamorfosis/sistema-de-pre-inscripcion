<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
			$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
            $coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
            $profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
            $visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
            $visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
            $visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;
$this->menu=array(
	array('label'=>Yii::t('app','List Users'), 'url'=>array('index'), 'visible'=>$visible),
	array('label'=>Yii::t('app','Create Users'), 'url'=>array('create'), 'visible'=>$visible),
	array('label'=>Yii::t('app','View Users'), 'url'=>array('view', 'id'=>$model->id), 'visible'=>$visible),
	array('label'=>Yii::t('app','Manage Users'), 'url'=>array('admin'), 'visible'=>$visible),
);
?>

<h1><?php echo Yii::t('app','Update').' '.$model->name.' '.$model->last_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
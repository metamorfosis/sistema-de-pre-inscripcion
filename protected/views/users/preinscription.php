<?php
/* @var $this SiteController */
/* @var $model Users */
/* @var $model ValidatePassword */
/* @var $form CActiveForm  */
$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	Yii::t('app', 'Preinscription'),
	);

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;

?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<h1><?php echo Yii::t('app','Preinscription'); ?></h1>
			<div class="form">
				<?php
					$form=$this->beginWidget('yiistrap.widgets.TbActiveForm',
						array(
							'method'=>'POST',
							'action'=>Yii::app()->createUrl('users/preinscription'),
							'id'=>'formPreinscription',
							'enableAjaxValidation'=>true,
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>true,
								'validateOnType'=>true,
								),
						)
					);
				?>
				<p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.') ?></p>
				<?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-error')); ?>
				<br>
				<div>
					<?php echo 'Usuario '.$user->name.' '.$user->last_name; ?>
				</div>
				<br>
				<div>
					<?php echo $form->dropDownListControlGroup($model,'semester_id', $semestres, array('empty'=>'Seleccione')); ?>
				</div>
				<div>
					<?php echo $form->textField($model,'user_id', array('size'=>60,'maxlength'=>255, 'value'=>$user_id, 'class' => 'hidden')); ?>
					<?php echo $form->textField($model,'pensum_id', array('size'=>60,'maxlength'=>255, 'value'=>$pensum, 'class' => 'hidden')); ?>
				</div>
				<div class="buttons">
					<?php echo CHtml::submitButton(Yii::t('app', 'Send'), array('class'=>'btn btn-primary btn-lg')); ?>
				</div>
				<?php $this->endWidget(); ?>
				<br>
			</div><!-- form -->
		</div>
	</div>
</div>




<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	Yii::t('app', 'Account'),
	);

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
$estudiante = (isset(Yii::app()->user->estudiante)) ? Yii::app()->user->estudiante : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
$visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
$visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

?>

	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="about">
				<div class="container">
					<div class="about-info">
						<h1>Mi Cuenta</h1><br>
						<div class="about-text">
							<h2>Datos Personales</h2>
							<p>
								<?php $this->widget('bootstrap.widgets.TbDetailView', array(
									'data'=>$model,
									'attributes'=>array(
										array('label'=>'Nombre', 'name'=>'name'),
										array('label'=>'Apellido', 'name'=>'last_name'),
										array('label'=>'C.I.', 'name'=>'ci'),
										array('label'=>'Correo', 'name'=>'email'),
										array('label'=>'phone', 'name'=>'phone'),
										array('label'=>'Dirección', 'name'=>'address'),
										array('label'=>'Cumpleaños', 'name'=>'birthday'),
										array('label'=>'Tipo', 'name'=>'type'),
										// array('label'=>'Pensum', 'value'=>$model->pensumu->cod),
										//array('label'=>'Ultimo Inicio de Sesión', 'name'=>'last_login'),
								    	),
									));
								?>
								<br>
								<?php
									echo "<b>".CHtml::link(Yii::t('app', 'Update Data'), array('update', 'id'=>$model->id),
										array('class'=>'btn btn-info btn-small'))."</b>";
								?>
								<?php
									echo "<b>".CHtml::link(Yii::t('app', 'Change Password'), array('changePass', 'id'=>$model->id),
										array('class'=>'btn btn-success btn-small'))."</b>";
								?>
								<?php
									/*if($estudiante){
										echo "<b>".CHtml::link('Pre-inscribir', array('preinscription'),
										array('class'=>'btn btn-warning btn-small'))."</b>";
									}*/
								?>
								<?php
								if($profesor || $admin){
									echo "<b>".CHtml::link('Disponibilidad', array('availability/create'),
										array('class'=>'btn btn-warning btn-small'))."</b>";
								}
								?>
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>


<br>
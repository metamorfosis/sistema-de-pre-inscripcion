<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="form">
				<?php
					$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',
						array(
							'method'=>'POST',
							'action'=>Yii::app()->createUrl('users/account'),
							'id'=>'formChangePass',
							'enableAjaxValidation'=>true,
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>true,
								'validateOnType'=>true,
							),
						)
					);
				?>
				<p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.') ?></p>
				<?php echo $form->errorSummary($pass,null,null,array('class'=>'alert alert-error')); ?>
				<div>
					<?php echo $form->textField($pass,'id', array('size'=>60,'maxlength'=>255, 'value'=>Yii::app()->user->id, 'class' => 'hidden')); ?>
					<?php echo $form->error($pass,'id'); ?>
				</div>
				<div>
					<?php echo $form->labelEx($pass,Yii::t('app','old')); ?><p class="help-block1"><span>|</span>Introduzca la Contraseña actual.</p>
					<?php echo $form->textField($pass,'old', array('size'=>60,'maxlength'=>255, 'placeholder'=>'Mi Contraseña Actual')); ?>
					<?php echo $form->error($pass,'old'); ?>
				</div>
				<br>
				<div>
					<?php echo $form->labelEx($pass,Yii::t('app','new')); ?><p class="help-block1"><span>|</span>Introduzca la Contraseña Nueva.</p>
					<?php echo $form->passwordField($pass,'new', array('size'=>60,'maxlength'=>255, 'placeholder'=>'Mi Contraseña Nueva')); ?>
					<?php echo $form->error($pass,'new'); ?>
				</div>
				<br>
				<div>
					<?php echo $form->labelEx($pass,Yii::t('app','new_repeat')); ?><p class="help-block1"><span>|</span>Repita la Contraseña Nueva.</p>
					<?php echo $form->passwordField($pass,'new_repeat', array('size'=>60,'maxlength'=>255, 'placeholder'=>'Repetir Mi Contraseña Nueva')); ?>
					<?php echo $form->error($pass,'new_repeat'); ?>
				</div>
				<br><br>
				<div class="buttons">
					<?php echo CHtml::submitButton(Yii::t('app', 'Change'), array('class'=>'btn btn-primary btn-lg')); ?>
				</div>
				<?php $this->endWidget(); ?>
				<br>
			</div><!-- form -->
		</div>
	</div>
</div>

<?php
	/*
	<div>
		<?php echo $form->labelEx($model,'password_id'); ?>
		<?php echo $form->textField($model,'password_id'); ?>
		<?php echo $form->error($model,'password_id'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'registration'); ?>
		<?php echo $form->textField($model,'registration'); ?>
		<?php echo $form->error($model,'registration'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'last_login'); ?>
		<?php echo $form->textField($model,'last_login'); ?>
		<?php echo $form->error($model,'last_login'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	 */
?>
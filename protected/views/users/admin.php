<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	Yii::t('app','Manage'),
);

	$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
	$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
	$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
	$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
	$visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
	$visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;
$this->menu=array(
	array('label'=>Yii::t('app','List Users'), 'url'=>array('index'), 'visible'=>$admin),
	array('label'=>Yii::t('app','Create Users'), 'url'=>array('create'), 'visible'=>$visible),
);
?>
<br>
<h1><?php echo Yii::t('app','Manage Users'); ?></h1><br>
<?php if($admin): ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'users-grid',
	'type'=>'striped bordered condensed',
	'template'=>"{items}",
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ci',
		'name',
		'last_name',
		'type',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
		),
	)
)); ?>
<?php else: ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'users-grid',
	'type'=>'striped bordered condensed',
	'template'=>"{items}",
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ci',
		'name',
		'last_name',
		'type',
		array(
			'class'=>'TbButtonColumn',
			'template'=>'{view}{update}',
		),
	)
)); ?>
<?php endif; ?>
<?php
		/*
		<div class="span12">
		<h1>Create Role</h1>
		<?php $form=$this->beginWidget("CActiveForm"); ?>
		<?php echo $form->labelEx($role,"name"); ?>
		<?php echo $form->textField($role,"name"); ?>
		<?php echo $form->error($role,"name"); ?>

		<?php echo $form->labelEx($role,"description"); ?>
		<?php echo $form->textArea($role,"description"); ?>
		<?php echo $form->error($role,"description"); ?>
		<br>
		<?php echo CHtml::submitButton("Create", array("class"=>"btn btn-primary btn-medium")); ?>
		<?php $this->endWidget(); ?>
	</div>
		 */
?>
<div class="container">
	<div class="row">
		<div class="col-xs-4">
			<h1>Roles</h1>
			<ul class="nav nav-tabs nav-stacked">
			<?php foreach (Yii::app()->authManager->getAuthItems() as $data): ?>
				<?php if ($data->name !== 'Admin' and $data->name !== 'Coordinador') { ?>
					<?php $enabled=Yii::app()->authManager->checkAccess($data->name, $model->id) ?>
					<li>
						<h4><?php echo $data->name ?>
							<span>&nbsp;-&nbsp;</span>
							<small>
								<?php if ($data->type==0)  echo "Role"; ?>
								<?php if ($data->type==1)  echo "Tarea"; ?>
								<?php if ($data->type==2)  echo "Operación"; ?>
							</small>
							&nbsp;<?php echo CHtml::link($enabled? "Off":"On", array("users/assign", "id"=>$model->id, "item"=>$data->name, "page"=>$page),
							array("class"=>$enabled?"btn btn-danger btn-small":"btn btn-success btn-small")); ?>
						</h4>
						<p>
							<?php #echo $enabled?'<span class="label label-important">Asignado</span>':""; ?>
							<?php echo $data->description ?>
						</p>
					</li>
				<?php } elseif (Yii::app()->user->admin == true) { ?>
					<?php $enabled=Yii::app()->authManager->checkAccess($data->name, $model->id) ?>
					<li>
						<h4><?php echo $data->name ?>
							<span>&nbsp;-&nbsp;</span>
							<small>
								<?php if ($data->type==0)  echo "Role"; ?>
								<?php if ($data->type==1)  echo "Tarea"; ?>
								<?php if ($data->type==2)  echo "Operación"; ?>
							</small>
							&nbsp;<?php echo CHtml::link($enabled? "Off":"On", array("users/assign", "id"=>$model->id, "item"=>$data->name, "page"=>$page),
							array("class"=>$enabled?"btn btn-danger btn-small":"btn btn-success btn-small")); ?>
						</h4>
						<p>
							<?php #echo $enabled?'<span class="label label-important">Asignado</span>':""; ?>
							<?php echo $data->description ?>
						</p>
					</li>
			<?php } endforeach; ?>
		</ul>
		</div>
	</div>
</div>

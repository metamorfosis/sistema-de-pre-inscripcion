<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
	$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
	$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div>
		<?php echo $form->textFieldRow($model,'ci'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'last_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->label($model,'birthdate'); ?>
		<?php
			$this->widget("zii.widgets.jui.CJuiDatePicker",array(
				"attribute"=>"birthdate",
				"model"=>$model,
				"language"=>"es",
				"options"=>array(
					'showButtonPanel'=>true,
					'changeYear'=>true,
					'changeYear'=>true,
					'dateFormat'=>'yy-mm-dd',
					'yearRange'=>'-80:-15',
					'minDate'=>'-80Y',
					'maxDate'=>'-15Y',
				)
			));
		?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'address',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'movil',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'username',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary btn-large')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
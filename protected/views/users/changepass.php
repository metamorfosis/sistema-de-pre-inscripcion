<?php
/* @var $this SiteController */
/* @var $model Users */
/* @var $model ValidatePassword */
/* @var $form CActiveForm  */
$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	Yii::t('app', 'Change Password'),
	);

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;

// $this->menu=array(
// 	array('label'=>Yii::t('app', 'Back'), 'url'=>array('account', 'id'=>$id)),
// 	);
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<h1><?php echo Yii::t('app','Change Password'); ?></h1>
			<div class="form">
				<?php
					$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',
						array(
							'method'=>'POST',
							'action'=>Yii::app()->createUrl('users/changepass'),
							'id'=>'formChangepass',
							'enableAjaxValidation'=>true,
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>true,
								'validateOnType'=>true,
								),
						)
					);
				?>
				<p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.') ?></p>
				<?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-error')); ?>
				<div>
					<?php echo $form->textField($model,'id', array('size'=>60,'maxlength'=>255, 'value'=>$id, 'class' => 'hidden')); ?>
					<?php echo $form->error($model,'id'); ?>
				</div>
				<div>
					<?php echo $form->labelEx($model,Yii::t('app','new password')); ?><!-- <p class="help-block1"><span>|</span>Introduzca la Contraseña Nueva.</p> -->
					<?php echo $form->passwordField($model,'new', array('size'=>60,'maxlength'=>255, 'placeholder'=>'Mi Contraseña Nueva')); ?>
					<?php echo $form->error($model,'new'); ?>
				</div>
				<br>
				<div>
					<?php echo $form->labelEx($model,Yii::t('app','repeat new password')); ?><!-- <p class="help-block1"><span>|</span>Repita la Contraseña Nueva.</p> -->
					<?php echo $form->passwordField($model,'new_repeat', array('size'=>60,'maxlength'=>255, 'placeholder'=>'Repetir Mi Contraseña Nueva')); ?>
					<?php echo $form->error($model,'new_repeat'); ?>
				</div>
				<br><br>
				<div class="buttons">
					<?php echo CHtml::submitButton(Yii::t('app', 'Change'), array('class'=>'btn btn-primary btn-lg')); ?>
				</div>
				<?php $this->endWidget(); ?>
				<br>
			</div><!-- form -->
		</div>
	</div>
</div>




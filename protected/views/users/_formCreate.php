<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
	$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
    $coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
	$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'usersform-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('app', 'Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
	<?php if($visible): ?>
	<div>
		<?php echo $form->textFieldRow($model,'ci', array('placeholder'=>'12345678')); ?>
	</div>
	<?php endif; ?>
	<div>
		<?php echo $form->textFieldRow($model,'name',array('placeholder'=>'Mi Nombre','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'last_name',array('placeholder'=>'Mi Apellido','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'birthday'); ?>
		<?php
			$this->widget("zii.widgets.jui.CJuiDatePicker",array(
				"attribute"=>"birthday",
				"model"=>$model,
				"language"=>"es",
				"options"=>array(
					'showButtonPanel'=>true,
					'changeYear'=>true,
					'dateFormat'=>'yy-mm-dd',
					'yearRange'=>'-80:-15',
					'minDate'=>'-80Y',
					'maxDate'=>'-15Y',
				)
			));
		?>
		<?php echo $form->error($model,'birthday'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'address',array('placeholder'=>'Mi Dirección','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'email',array('placeholder'=>'MiCorreo@algo.com','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'phone',array('placeholder'=>'0123-456-78-90','size'=>60,'maxlength'=>255)); ?>
	</div>

	<?php // Mostrar Tipo de Usuario según Role ?>
	<?php if ($admin): ?>
		<div>
			<?php echo $form->dropDownListRow($model,'type',$model->getMenuAsing(2), array("empty"=>"Seleccione")); ?>
			<?php echo $form->error($model,'type'); ?>
		</div>
	<?php elseif($coordinador): ?>
		<div>
			<?php echo $form->dropDownListRow($model,'type',$model->getMenuAsing(1), array("empty"=>"Seleccione")); ?>
			<?php echo $form->error($model,'type'); ?>
		</div>
	<?php endif ?>

	<div>
		<?php echo $form->labelEx($model,'pensum_id'); ?>
		<?php
			 echo $form->hiddenField($model,'pensum_id');
			 $this->widget('zii.widgets.jui.CJuiAutoComplete',
			 			  array(
			 				  'name'=>'pensum_id_new',
			 				  'model'=>$model,
			 				  'value'=>$model->isNewRecord ? '' : $model->pensum_id,
			 				  'source'=>$this->createUrl('users/autocomplete'),
			 				  'options'=> array(
			 					  'showAnim'=>'fold',
			 					  'size'=>'30',
			 					  'minLength'=>'2',
			 					  'select'=>"js:function(event, ui) {
			 							$('#Users_pensum_id').val(ui.item.id);
			 						}"
			 				  ),
			 				  'htmlOptions'=> array(
			 					  'size'=>60,
			 					  'placeholder'=>'Pensum',
			 					  'title'=>'Indique el Pensum.'
			 				  ),
			 			  )
			 );
		?>
		<?php echo $form->error($model,'pensum_id'); ?>
	</div>
	<br>
	<div class="buttons">
		<?php echo CHtml::submitButton(Yii::t('app', 'Create'), array('class'=>'btn btn-primary btn-large')); ?>
	</div>
	<br>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this SiteController */
/* @var $model Users */
/* @var $model ValidatePassword */
/* @var $form CActiveForm  */
$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	Yii::t('app', 'Preinscription'),
	);

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;

// $this->menu=array(
// 	array('label'=>Yii::t('app', 'Back'), 'url'=>array('account', 'id'=>$id)),
// 	);
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<h1><?php echo Yii::t('app','Availability'); ?></h1>
			<div class="form">
				<?php
					$form=$this->beginWidget('yiistrap.widgets.TbActiveForm',
						array(
							'method'=>'POST',
							'action'=>Yii::app()->createUrl('users/availability'),
							'id'=>'formAvailability',
							'enableAjaxValidation'=>true,
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>true,
								'validateOnType'=>true,
								),
						)
					);
				?>
				<p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.') ?></p>
				<?php echo $form->errorSummary($model,null,null,array('class'=>'alert alert-error')); ?>
				<br>
				<div>
					<?php echo 'Usuario '.Yii::app()->user->name; ?>
				</div>
				<br>
				<div>
					<?php echo $form->labelEx($model,'matter_id'); ?>
					<?php
						echo $form->hiddenField($model,'matter_id'); 
						$this->widget('zii.widgets.jui.CJuiAutoComplete',
									  array(
										  'name'=>'matter_id_new',
										  'model'=>$model,
										  'value'=>$model->isNewRecord ? '' : $model->matter_id,
										  'source'=>$this->createUrl('users/autocompletematters'),
										  'options'=> array(
											  'showAnim'=>'fold',
											  'size'=>'30',
											  'minLength'=>'2',
											  'select'=>"js:function(event, ui) {
														$('#Availability_matter_id').val(ui.item.id);
													}"
										  ),
										  'htmlOptions'=> array(
											  'size'=>60,
											  'placeholder'=>'Materia',
											  'title'=>'Indique la Materia.'
										  ),
									  )
						);
					?>
					<?php echo $form->error($model,'matter_id'); ?>
				</div>
				<br>
				<div>
					<?php echo $form->dropDownListControlGroup($model,'day', $dia, array('empty'=>'Seleccione')); ?>
				</div>
				<br>
				<div>
					<?php echo $form->dropDownListControlGroup($model,'hour_start', $horas, array('empty'=>'Seleccione')); ?>
				</div>
				<br>
				<div>
					<?php echo $form->dropDownListControlGroup($model,'hour_end', $horas, array('empty'=>'Seleccione')); ?>
				</div>
				<div>
					<?php echo $form->textField($model,'user_id', array('size'=>60,'maxlength'=>255, 'value'=>$id, 'class' => 'hidden')); ?>
				</div>
				<div class="buttons">
					<?php echo CHtml::submitButton(Yii::t('app', 'Send'), array('class'=>'btn btn-primary btn-lg')); ?>
				</div>
				<?php $this->endWidget(); ?>
				<br>
			</div><!-- form -->
		</div>
	</div>
</div>




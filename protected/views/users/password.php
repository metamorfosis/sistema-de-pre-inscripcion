<?php
/* @var $this UsersController */
/* @var $model Users */

	$this->breadcrumbs=array(
		Yii::t('app','Users')=>array('index'),
		Yii::t('app','Create'),
	);

	$this->menu=array(
		array('label'=>Yii::t('app','List Users'), 'url'=>array('index'), 'visible'=>$visible),
		array('label'=>Yii::t('app','Manage Users'), 'url'=>array('admin'), 'visible'=>$visible),
	);
?>

<h1><?php echo Yii::t('app','Create Users') ?></h1>

<div class="container">
	<div class="row">
		<div class="col-xs-8">
			<?php $this->renderPartial('_pass', array('model'=>$model)); ?>
		</div>
		</div>
	</div>
</div>
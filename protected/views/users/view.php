<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	Yii::t('app','Users')=>array('index'),
	$model->name,
);
			$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
            $coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
            $profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
            $visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
            $visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
            $visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;
$this->menu=array(
	array('label'=>Yii::t('app','List Users'), 'url'=>array('index'), 'visible'=>$visible),
	array('label'=>Yii::t('app','Create Users'), 'url'=>array('create'), 'visible'=>$visible),
	array('label'=>Yii::t('app','Update Users'), 'url'=>array('update', 'id'=>$model->id), 'visible'=>$visible ),
	array('label'=>Yii::t('app','Delete Users'), 'url'=>'#', 'visible'=>$admin, 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Users'), 'url'=>array('admin'), 'visible'=>$visible),
);
?>
<br>
<div class="container">
	<div>
		<div class="col-xs-10">
		<h1> <?php echo $model->name.' '.$model->last_name; ?></h1>
		<?php $this->widget('bootstrap.widgets.TbDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'ci',
				'birthday',
				'address',
				'email',
				'phone',
				'username',
				'type',
				/*array(
					'name'=>'pensum_id',
					'value'=>$model->pensumu->cod,
				),*/
				array(
					'name'=>'status_id',
					'value'=>$model->statusu->name,
				),

				)
		)); ?>
		</div>
	</div>
</div>
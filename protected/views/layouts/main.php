<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="es">

	<!-- Bootstrap CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" media="all">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php

            $admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
			$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
			$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
            $visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
            $visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
            $visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

			$this->widget('yiistrap.widgets.TbNav',array(
				'type' => TbHtml::NAV_TYPE_PILLS,
				'items'=>array(
					array('label'=>Yii::t('app','Home'), 'url'=>array('/site/index')),
					array('label'=>Yii::t('app','Users'), 'items'=>array(
						array('label'=>Yii::t('app','View'), 'url'=>array('/users/index'), 'visible'=>$visible3),
					),
					array('label'=>Yii::t('app','Career'), 'url'=>array('/career/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Matter'), 'url'=>array('/matter/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Studentlist'), 'url'=>array('/studentlist/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Classroom'), 'url'=>array('/classroom/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Semester'), 'url'=>array('/semester/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Pensum'), 'url'=>array('/pensum/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Schedules'), 'url'=>array('/schedules/index'), 'visible'=>$visible),
					array('label'=>Yii::t('app','Contact'), 'url'=>array('/site/contact')),
					array('label'=>Yii::t('app','Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>Yii::t('app','Logout').' ( '.Yii::app()->user->name.' )', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
				),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('yiistrap.widgets.TbBreadcrumb', array(
    							'htmlOptions'=>array("style"=>"margin: 10px 0;"),
    							'links'=>$this->breadcrumbs,
    							)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by los Unefistas.<br/>
		Todos los Derechos Reservados.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php
				$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>$this->menu,
					'htmlOptions'=>array('class'=>'nav nav-pills'),
				));
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-11">
			<?php echo $content; ?>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>
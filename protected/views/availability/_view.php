<?php
/* @var $this AvailabilityController */
/* @var $data Availability */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('days')); ?>:</b>
	<?php echo CHtml::encode($data->days); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hour_start')); ?>:</b>
	<?php echo CHtml::encode($data->hour_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hour_end')); ?>:</b>
	<?php echo CHtml::encode($data->hour_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matter_id')); ?>:</b>
	<?php echo CHtml::encode($data->matter_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />


</div>
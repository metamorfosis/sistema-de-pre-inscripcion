<?php
/* @var $this AvailabilityController */
/* @var $model Availability */

$this->breadcrumbs=array(
    Yii::t('app','Availabilities')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Availability'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Availability'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Availability').' #'.$model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
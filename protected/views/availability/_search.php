<?php
/* @var $this AvailabilityController */
/* @var $model Availability */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'days'); ?>
		<?php echo $form->textField($model,'days',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hour_start'); ?>
		<?php echo $form->textField($model,'hour_start'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hour_end'); ?>
		<?php echo $form->textField($model,'hour_end'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter_id'); ?>
		<?php echo $form->textField($model,'matter_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this AvailabilityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Availabilities'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Availability'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Availability'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Availabilities'); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this AvailabilityController */
/* @var $model Availability */

$this->breadcrumbs=array(
	'Availabilities'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Availability'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Availability'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Availability'), 'url'=>array('admin')),
);
?>

<h1>View Availability #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'name'=>'days',
            'value'=>$model->day->name,
        ),
        array(
            'name'=>'hour_start',
            'value'=>$model->hourStart->name,
        ),
        array(
            'name'=>'hour_end',
            'value'=>$model->hourStart->name,
        ),
        array(
            'name'=>'matter_id',
            'value'=>$model->matter->name,
        ),
        array(
            'name'=>'user_id',
            'value'=>$model->user->name,
        ),
	),
)); ?>

<?php
/* @var $this AvailabilityController */
/* @var $model Availability */

$this->breadcrumbs=array(
    Yii::t('app','Availabilities')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Availability'), 'url'=>array('create')),
);

?>

<h1><?php echo Yii::t('app','Manage Availabilities'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'availability-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'days',
		'hour_start',
		'hour_end',
		'matter',
		'user',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this AvailabilityController */
/* @var $model Availability */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'availability-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('app', 'Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div>
        <?php echo $form->dropDownListRow($model,'days',$model->getDays(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'days'); ?>
	</div>

	<div>
        <?php echo $form->dropDownListRow($model,'hour_start',$model->getMenuHours(2), array("empty"=>"Seleccione")); ?>
		<?php echo $form->error($model,'hour_start'); ?>
	</div>

	<div>
        <?php echo $form->dropDownListRow($model,'hour_end',$model->getMenuHours(2), array("empty"=>"Seleccione")); ?>
		<?php echo $form->error($model,'hour_end'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter_id'); ?>
        <?php
        echo $form->hiddenField($model,'matter_id');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'matter_id_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->matter_id,
                'source'=>$this->createUrl('availability/autocomplete'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) {
										$('#Avalability_matter_id').val(ui.item.id);
									}"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'Código de Materia',
                    'title'=>'Indique el Materia.'
                ),
            )
        );

        ?>
		<?php echo $form->error($model,'matter_id'); ?>
	</div>
    <br>
	<div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('class'=>'btn btn-primary btn-large')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
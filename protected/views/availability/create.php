<?php
/* @var $this AvailabilityController */
/* @var $model Availability */

$this->breadcrumbs=array(
    Yii::t('app','Availabilities')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Availability'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Availability'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Availability'); ?></h1>
    <br>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
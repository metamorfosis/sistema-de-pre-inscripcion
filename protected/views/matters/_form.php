<?php
/* @var $this MattersController */
/* @var $model Matters */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'matters-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->textFieldRow($model,'cod',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cod'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div>
        <?php echo $form->labelEx($model,'priority1'); ?>
		<?php echo $form->hiddenField($model,'priority1');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'priority1_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->cod,
                'source'=>$this->createUrl('matters/autocomplete'), // URL que genera el conjunto de datos
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) {
                                $('#Matters_priority1').val(ui.item.id);
                                }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'Código de Materia - Prelación 1',
                    'title'=>'Indique el Código de Materia.'
                ),
            )
        ); ?>
		<?php echo $form->error($model,'priority1'); ?>
	</div>

	<div>
        <?php echo $form->labelEx($model,'priority2'); ?>
		<?php echo $form->hiddenField($model,'priority2');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'priority2_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->cod,
                'source'=>$this->createUrl('matters/autocomplete'), // URL que genera el conjunto de datos
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) {
                                $('#Matters_priority2').val(ui.item.id);
                                }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'Código de Materia - Prelación 2',
                    'title'=>'Indique el Código de Materia.'
                ),
            )
        ); ?>
		<?php echo $form->error($model,'priority2'); ?>
	</div>

	<div>
        <?php echo $form->textFieldRow($model,'unitsCredit'); ?>
		<?php echo $form->error($model,'unitsCredit'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'hoursLab',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hoursLab'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'hoursTheory',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hoursTheory'); ?>
	</div>

	<div>
		<?php echo $form->textFieldRow($model,'hoursPractice',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hoursPractice'); ?>
	</div>

	<div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('class'=>'btn btn-primary btn-lg')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
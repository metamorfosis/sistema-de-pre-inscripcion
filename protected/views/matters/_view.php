<?php
/* @var $this MattersController */
/* @var $data Matters */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod')); ?>:</b>
	<?php echo CHtml::encode($data->cod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority1')); ?>:</b>
	<?php echo CHtml::encode($data->priority1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority2')); ?>:</b>
	<?php echo CHtml::encode($data->priority2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unitsCredit')); ?>:</b>
	<?php echo CHtml::encode($data->unitsCredit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hoursLab')); ?>:</b>
	<?php echo CHtml::encode($data->hoursLab); ?>
	<br />

</div>
<hr>
<?php
/* @var $this MattersController */
/* @var $model Matters */

$this->breadcrumbs=array(
    Yii::t('app','Matters')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Matters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Matters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Matters'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
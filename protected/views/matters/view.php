<?php
/* @var $this MattersController */
/* @var $model Matters */

$this->breadcrumbs=array(
	'Matters'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Matters', 'url'=>array('index')),
	array('label'=>'Create Matters', 'url'=>array('create')),
	array('label'=>'Update Matters', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Matters', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Matters', 'url'=>array('admin')),
);
?>

<h1>View Matters #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cod',
		'name',
		'priority1',
		'priority2',
		'unitsCredit',
		'hoursLab',
		'hoursTheory',
		'hoursPractice',
	),
)); ?>

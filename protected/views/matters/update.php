<?php
/* @var $this MattersController */
/* @var $model Matters */

$this->breadcrumbs=array(
    Yii::t('app','Matters')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Matters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Matters'), 'url'=>array('create')),
	array('label'=>Yii::t('app','View Matters'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Matters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Matters').' '.$model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
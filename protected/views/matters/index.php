<?php
/* @var $this MattersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Matters'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Matters'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Matters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Matters'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this MattersController */
/* @var $model Matters */

$this->breadcrumbs=array(
    Yii::t('app','Matters')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Matters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Matters'), 'url'=>array('create')),
);

?>

<h1><?php echo Yii::t('app','Manage Matters'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'matters-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cod',
		'name',
		'priority1',
		'priority2',
		'unitsCredit',
		/*
		'hoursLab',
		'hoursTheory',
		'hoursPractice',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

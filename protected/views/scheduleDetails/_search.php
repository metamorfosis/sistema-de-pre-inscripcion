<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'schedule_id'); ?>
		<?php echo $form->textField($model,'schedule_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'day'); ?>
		<?php echo $form->textField($model,'day'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block1'); ?>
		<?php echo $form->textField($model,'block1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block2'); ?>
		<?php echo $form->textField($model,'block2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block3'); ?>
		<?php echo $form->textField($model,'block3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block4'); ?>
		<?php echo $form->textField($model,'block4'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block5'); ?>
		<?php echo $form->textField($model,'block5'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block6'); ?>
		<?php echo $form->textField($model,'block6'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block7'); ?>
		<?php echo $form->textField($model,'block7'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block8'); ?>
		<?php echo $form->textField($model,'block8'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block9'); ?>
		<?php echo $form->textField($model,'block9'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block10'); ?>
		<?php echo $form->textField($model,'block10'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block11'); ?>
		<?php echo $form->textField($model,'block11'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block12'); ?>
		<?php echo $form->textField($model,'block12'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'block13'); ?>
		<?php echo $form->textField($model,'block13'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user1_id'); ?>
		<?php echo $form->textField($model,'user1_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user2_id'); ?>
		<?php echo $form->textField($model,'user2_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user3_id'); ?>
		<?php echo $form->textField($model,'user3_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user4_id'); ?>
		<?php echo $form->textField($model,'user4_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user5_id'); ?>
		<?php echo $form->textField($model,'user5_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user6_id'); ?>
		<?php echo $form->textField($model,'user6_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user7_id'); ?>
		<?php echo $form->textField($model,'user7_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user8_id'); ?>
		<?php echo $form->textField($model,'user8_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user9_id'); ?>
		<?php echo $form->textField($model,'user9_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user10_id'); ?>
		<?php echo $form->textField($model,'user10_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user11_id'); ?>
		<?php echo $form->textField($model,'user11_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user12_id'); ?>
		<?php echo $form->textField($model,'user12_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user13_id'); ?>
		<?php echo $form->textField($model,'user13_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */

$this->breadcrumbs=array(
    Yii::t('app','Schedules')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List Schedules'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Schedules'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Schedules'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete Schedules'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Schedules'), 'url'=>array('admin')),
);
?>

<h1>View ScheduleDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'schedule_id',
		'day',
		'block1',
		'block2',
		'block3',
		'block4',
		'block5',
		'block6',
		'block7',
		'block8',
		'block9',
		'block10',
		'block11',
		'block12',
		'block13',
		'user1_id',
		'user2_id',
		'user3_id',
		'user4_id',
		'user5_id',
		'user6_id',
		'user7_id',
		'user8_id',
		'user9_id',
		'user10_id',
		'user11_id',
		'user12_id',
		'user13_id',
	),
)); ?>

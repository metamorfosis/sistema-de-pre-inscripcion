<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */

$this->breadcrumbs=array(
    Yii::t('app','Schedules')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
    array('label'=>Yii::t('app','List Schedules'), 'url'=>array('index')),
    array('label'=>Yii::t('app','Create Schedules'), 'url'=>array('create')),
);
?>

<h1><?php echo Yii::t('app','Manage Schedules'); ?></h1>
<br>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'schedule-details-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'schedule_id',
		'day',
		'block1',
		'block2',
		'block3',
		/*
		'block4',
		'block5',
		'block6',
		'block7',
		'block8',
		'block9',
		'block10',
		'block11',
		'block12',
		'block13',
		'user1_id',
		'user2_id',
		'user3_id',
		'user4_id',
		'user5_id',
		'user6_id',
		'user7_id',
		'user8_id',
		'user9_id',
		'user10_id',
		'user11_id',
		'user12_id',
		'user13_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

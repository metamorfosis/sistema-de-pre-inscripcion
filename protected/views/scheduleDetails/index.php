<?php
/* @var $this ScheduleDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Schedules'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Schedules'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Schedules'), 'url'=>array('admin')),

);
?>

<h1><?php echo Yii::t('app','Schedules'); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

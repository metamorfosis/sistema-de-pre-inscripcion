<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */

$this->breadcrumbs=array(
    Yii::t('app','Schedules')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Schedules'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Schedules'), 'url'=>array('create')),
	array('label'=>Yii::t('app','View Schedules'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Schedules'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Schedules').' - Dia '.$days[$day]; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ScheduleDetailsController */
/* @var $data ScheduleDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('schedule_id')); ?>:</b>
	<?php echo CHtml::encode($data->schedule_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day')); ?>:</b>
	<?php echo CHtml::encode($data->day); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block1')); ?>:</b>
	<?php echo CHtml::encode($data->block1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block2')); ?>:</b>
	<?php echo CHtml::encode($data->block2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block3')); ?>:</b>
	<?php echo CHtml::encode($data->block3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block4')); ?>:</b>
	<?php echo CHtml::encode($data->block4); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('block5')); ?>:</b>
	<?php echo CHtml::encode($data->block5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block6')); ?>:</b>
	<?php echo CHtml::encode($data->block6); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block7')); ?>:</b>
	<?php echo CHtml::encode($data->block7); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block8')); ?>:</b>
	<?php echo CHtml::encode($data->block8); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block9')); ?>:</b>
	<?php echo CHtml::encode($data->block9); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block10')); ?>:</b>
	<?php echo CHtml::encode($data->block10); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block11')); ?>:</b>
	<?php echo CHtml::encode($data->block11); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block12')); ?>:</b>
	<?php echo CHtml::encode($data->block12); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('block13')); ?>:</b>
	<?php echo CHtml::encode($data->block13); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user1_id')); ?>:</b>
	<?php echo CHtml::encode($data->user1_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user2_id')); ?>:</b>
	<?php echo CHtml::encode($data->user2_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user3_id')); ?>:</b>
	<?php echo CHtml::encode($data->user3_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user4_id')); ?>:</b>
	<?php echo CHtml::encode($data->user4_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user5_id')); ?>:</b>
	<?php echo CHtml::encode($data->user5_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user6_id')); ?>:</b>
	<?php echo CHtml::encode($data->user6_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user7_id')); ?>:</b>
	<?php echo CHtml::encode($data->user7_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user8_id')); ?>:</b>
	<?php echo CHtml::encode($data->user8_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user9_id')); ?>:</b>
	<?php echo CHtml::encode($data->user9_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user10_id')); ?>:</b>
	<?php echo CHtml::encode($data->user10_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user11_id')); ?>:</b>
	<?php echo CHtml::encode($data->user11_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user12_id')); ?>:</b>
	<?php echo CHtml::encode($data->user12_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user13_id')); ?>:</b>
	<?php echo CHtml::encode($data->user13_id); ?>
	<br />

	*/ ?>

</div>
<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */

$this->breadcrumbs=array(
    Yii::t('app','Schedule Details')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Schedules'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Schedules'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Schedules').' - Dia '.$days[$day]; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'id'=>$id, 'day'=>$day)); ?>
<?php
/* @var $this ScheduleDetailsController */
/* @var $model ScheduleDetails */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'schedules-form',
    'type'=>'inline',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->hiddenField($model,'schedule_id', array('value'=>$id, 'readonly'=>'true')); ?>
		<?php echo $form->error($model,'schedule_id'); ?>
	</div>
	<div>
		<?php echo $form->hiddenField($model,'day', array('value'=>$day, 'readonly'=>'true')); ?>
		<?php echo $form->error($model,'day'); ?>
	</div>
    <hr>
	<div>
        <?php
            echo $form->hiddenField($model,'block1');
            $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block1_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block1,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block1').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'7:30 - 8:15',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
        <?php echo $form->error($model,'block1'); ?>
        <?php echo $form->dropDownListRow($model,'user1_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user1_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block2');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block2_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block2,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block2').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'8:15 - 9:00',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
        <?php echo $form->error($model,'block2'); ?>
        <?php echo $form->dropDownListRow($model,'user2_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user2_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block3');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block3_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block3,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block3').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'9:00 - 9:45',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block3'); ?>
        <?php echo $form->dropDownListRow($model,'user3_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user3_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block4');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block4_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block4,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#scheduleDetails_block4').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'9:45 - 10:30',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block4'); ?>
        <?php echo $form->dropDownListRow($model,'user4_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user4_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block5');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block5_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block5,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block5').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'10:30 - 11:15',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block5'); ?>
        <?php echo $form->dropDownListRow($model,'user5_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user5_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block6');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block6_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block6,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block6').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'11:15 - 12:00',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block6'); ?>
        <?php echo $form->dropDownListRow($model,'user6_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user6_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block7');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block7_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block7,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block7').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'12:00 - 1:00',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block7'); ?>
        <?php echo $form->dropDownListRow($model,'user7_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user7_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block8');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block8_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block8,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block8').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'1:00 - 1:45',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block8'); ?>
        <?php echo $form->dropDownListRow($model,'user8_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user8_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block9');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block9_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block9,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block9').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'1:45 - 2:30',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block9'); ?>
        <?php echo $form->dropDownListRow($model,'user9_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user9_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block10');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block10_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block10,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block10').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'2:30 - 3:15',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block10'); ?>
        <?php echo $form->dropDownListRow($model,'user10_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user10_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block11');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block11_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block11,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block11').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'3:15 - 4:00',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block11'); ?>
        <?php echo $form->dropDownListRow($model,'user11_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user11_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block12');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block12_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block12,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block12').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'4:00 - 4:45',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block12'); ?>
        <?php echo $form->dropDownListRow($model,'user12_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user12_id'); ?>
	</div>
    <hr>
	<div>
        <?php
        echo $form->hiddenField($model,'block13');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'block13_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->block13,
                'source'=>$this->createUrl('scheduleDetails/autocompletematter'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) { $('#ScheduleDetails_block13').val(ui.item.id); }"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'4:45 - 5:30',
                    'title'=>'Indique la Materia.'
                ),
            )
        );
        ?>
		<?php echo $form->error($model,'block13'); ?>
        <?php echo $form->dropDownListRow($model,'user13_id',$model->getTeachers(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'user13_id'); ?>
	</div>
    <hr>

    <div class="buttons">
        <?php echo CHtml::submitButton(Yii::t('app','Create'), array('class'=>'btn btn-primary btn-lg')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
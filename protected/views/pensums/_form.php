<?php
/* @var $this PensumsController */
/* @var $model Pensums */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'pensums-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('app', 'Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->textFieldRow($model,'year'); ?>
		<?php echo $form->error($model,'year'); ?>
	</div>

	<div>
        <?php echo $form->dropDownListRow($model,'turn',$model->getMenuTurn(), array("empty"=>"Seleccione")); ?>
        <?php echo $form->error($model,'turn'); ?>
	</div>
    <div>
        <?php echo $form->labelEx($model,'career_id'); ?>
        <?php
        echo $form->hiddenField($model,'career_id');
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>'career_id_new',
                'model'=>$model,
                'value'=>$model->isNewRecord ? '' : $model->career_id,
                'source'=>$this->createUrl('Pensums/autocomplete'),
                'options'=> array(
                    'showAnim'=>'fold',
                    'size'=>'30',
                    'minLength'=>'2',
                    'select'=>"js:function(event, ui) {
										$('#Pensums_career_id').val(ui.item.id);
									}"
                ),
                'htmlOptions'=> array(
                    'size'=>60,
                    'placeholder'=>'Carrera',
                    'title'=>'Indique la Carrera.'
                ),
            )
        );
        ?>
        <?php echo $form->error($model,'career_id'); ?>
    </div>

	<div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('class'=>'btn btn-primary btn-large')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
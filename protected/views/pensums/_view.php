<?php
/* @var $this PensumsController */
/* @var $data Pensums */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('turn')); ?>:</b>
	<?php echo CHtml::encode($data->turn0->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('career_id')); ?>:</b>
	<?php echo CHtml::encode($data->career->name); ?>
	<br />

</div>
<hr>
<?php
/* @var $this PensumsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Pensums'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Pensums'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Pensums'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Pensums'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

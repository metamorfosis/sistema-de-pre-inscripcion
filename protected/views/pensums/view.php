<?php
/* @var $this PensumsController */
/* @var $model Pensums */

$this->breadcrumbs=array(
    Yii::t('app','Pensums')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Pensums'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Pensums'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View Pensums').' #'. $model->id; ?></h1>
<br>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'year',
        array(
            'name'=>'turn',
            'value'=>$model->turn0->name,
        ),
        array(
            'name'=>'career_id',
            'value'=>$model->career->name,
        ),
	),
)); ?>

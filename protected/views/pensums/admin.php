<?php
/* @var $this PensumsController */
/* @var $model Pensums */

$this->breadcrumbs=array(
    Yii::t('app','Pensums')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Pensums'), 'url'=>array('create')),
);
?>

<h1><?php echo Yii::t('app','Manage Pensums'); ?></h1>

<br>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'pensums-grid',
    'type'=>'striped bordered condensed',
    'template'=>"{items}",
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'year',
        array(
            'name'=>'turn',
            'value'=>'$data->turn0->name',
            'type'=>'text',
        ),
        array(
            'name'=>'career_id',
            'value'=>'$data->career->name',
            'type'=>'text',
        ),
	),
)); ?>

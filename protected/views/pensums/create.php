<?php
/* @var $this PensumsController */
/* @var $model Pensums */

$this->breadcrumbs=array(
    Yii::t('app','Pensums')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Manage Pensums'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Pensums'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
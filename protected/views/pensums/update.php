<?php
/* @var $this PensumsController */
/* @var $model Pensums */

$this->breadcrumbs=array(
    Yii::t('app','Pensums')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Pensums'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Pensums'), 'url'=>array('create')),
	array('label'=>Yii::t('app','View Pensums'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Pensums'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Pensums').' # '.$model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
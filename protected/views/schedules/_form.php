<?php
/* @var $this SchedulesController */
/* @var $model Schedules */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
	<div class="span12">
		<div class="form">

			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'schedules-form',
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
			)); ?>

            <p class="note"><?php echo Yii::t('app', 'Fields with <span class="required">*</span> are required.') ?></p>

				<?php echo $form->errorSummary($model); ?>

				<div>
					<?php echo $form->labelEx($model,'classroom_id'); ?>
					<?php
						echo $form->hiddenField($model,'classroom_id');
						$this->widget('zii.widgets.jui.CJuiAutoComplete',
									  array(
										  'name'=>'classroom_id_new',
										  'model'=>$model,
										  'value'=>$model->isNewRecord ? '' : $model->classroom_id,
										  'source'=>$this->createUrl('schedules/autocomplete'),
										  'options'=> array(
											  'showAnim'=>'fold',
											  'size'=>'30',
											  'minLength'=>'1',
											  'select'=>"js:function(event, ui) {
													$('#Schedules_classroom_id').val(ui.item.id);
												}"
										  ),
										  'htmlOptions'=> array(
											  'size'=>60,
											  'placeholder'=>'Aula',
											  'title'=>'Indique el Aula.'
										  ),
									  )
						);
					?>
					<?php //echo $form->textField($model,'classroom_id'); ?>
					<?php echo $form->error($model,'classroom_id'); ?>
				</div>

				<div>
					<?php echo $form->labelEx($model,'semester_id'); ?>
					<?php
						echo $form->hiddenField($model,'semester_id');
						$this->widget('zii.widgets.jui.CJuiAutoComplete',
									  array(
										  'name'=>'semester_id_new',
										  'model'=>$model,
										  'value'=>$model->isNewRecord ? '' : $model->semester_id,
										  'source'=>$this->createUrl('schedules/autocompletesemester'),
										  'options'=> array(
											  'showAnim'=>'fold',
											  'size'=>'30',
											  'minLength'=>'2',
											  'select'=>"js:function(event, ui) {
													$('#Schedules_semester_id').val(ui.item.id);
												}"
										  ),
										  'htmlOptions'=> array(
											  'size'=>60,
											  'placeholder'=>'Semestre',
											  'title'=>'Indique el Semestre.'
										  ),
									  )
						);
					?>
					<?php echo $form->error($model,'semester_id'); ?>
				</div>

				<div class="buttons">
					<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Save'), array('class'=>'btn btn-primary btn-lg')); ?>
				</div>

			<?php $this->endWidget(); ?>

			</div>
	</div>
</div>
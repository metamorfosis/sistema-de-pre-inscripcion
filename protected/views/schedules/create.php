<?php
/* @var $this SchedulesController */
/* @var $model Schedules */

$this->breadcrumbs=array(
	Yii::t('app','Schedules')=>array('index'),
	yii::t('app','Create'),
);

/*$this->menu=array(
	array('label'=>'List Schedules', 'url'=>array('index')),
	array('label'=>'Manage Schedules', 'url'=>array('admin')),
);*/
?>

<h1><?php echo Yii::t('app','Create Schedules'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>


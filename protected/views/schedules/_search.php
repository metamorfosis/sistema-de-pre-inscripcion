<?php
/* @var $this SchedulesController */
/* @var $model Schedules */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'classroom_id'); ?>
		<?php echo $form->textField($model,'classroom_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'semester_id'); ?>
		<?php echo $form->textField($model,'semester_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this SchedulesController */
/* @var $data Schedules */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('classroom_id')); ?>:</b>
	<?php echo CHtml::encode($data->classroom->cod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('semester_id')); ?>:</b>
	<?php echo CHtml::encode($data->semester->name); ?>
	<br />

</div>
<hr>
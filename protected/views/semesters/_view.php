<?php
/* @var $this SemestersController */
/* @var $data Semesters */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->name), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('career_id')); ?>:</b>
	<?php echo CHtml::encode($data->career->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pensum_id')); ?>:</b>
	<?php echo CHtml::encode($data->pensum->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matter')); ?>:</b>
	<?php echo CHtml::encode($data->matter0->cod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('matter1')); ?>:</b>
	<?php echo CHtml::encode($data->matter10->cod); ?>
	<br />
	<hr />
</div>
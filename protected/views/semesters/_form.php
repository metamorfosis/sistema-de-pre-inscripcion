<?php
/* @var $this SemestersController */
/* @var $model Semesters */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'semesters-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('app','Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'career_id'); ?>
		<?php echo $form->textField($model,'career_id'); ?>
		<?php echo $form->error($model,'career_id'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'pensum_id'); ?>
		<?php echo $form->textField($model,'pensum_id'); ?>
		<?php echo $form->error($model,'pensum_id'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter'); ?>
		<?php echo $form->textField($model,'matter'); ?>
		<?php echo $form->error($model,'matter'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter1'); ?>
		<?php echo $form->textField($model,'matter1'); ?>
		<?php echo $form->error($model,'matter1'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter2'); ?>
		<?php echo $form->textField($model,'matter2'); ?>
		<?php echo $form->error($model,'matter2'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter3'); ?>
		<?php echo $form->textField($model,'matter3'); ?>
		<?php echo $form->error($model,'matter3'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter4'); ?>
		<?php echo $form->textField($model,'matter4'); ?>
		<?php echo $form->error($model,'matter4'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter5'); ?>
		<?php echo $form->textField($model,'matter5'); ?>
		<?php echo $form->error($model,'matter5'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter6'); ?>
		<?php echo $form->textField($model,'matter6'); ?>
		<?php echo $form->error($model,'matter6'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter7'); ?>
		<?php echo $form->textField($model,'matter7'); ?>
		<?php echo $form->error($model,'matter7'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter8'); ?>
		<?php echo $form->textField($model,'matter8'); ?>
		<?php echo $form->error($model,'matter8'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'matter9'); ?>
		<?php echo $form->textField($model,'matter9'); ?>
		<?php echo $form->error($model,'matter9'); ?>
	</div>

	<div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('class'=>'btn btn-primary btn-large')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
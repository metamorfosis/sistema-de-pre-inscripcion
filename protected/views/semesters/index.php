<?php
/* @var $this SemestersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('app','Semesters'),
);

$this->menu=array(
	array('label'=>Yii::t('app','Create Semesters'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage Semesters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Semesters'); ?></h1>
<br>
<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

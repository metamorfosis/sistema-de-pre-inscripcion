<?php
/* @var $this SemestersController */
/* @var $model Semesters */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'career_id'); ?>
		<?php echo $form->textField($model,'career_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pensum_id'); ?>
		<?php echo $form->textField($model,'pensum_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter'); ?>
		<?php echo $form->textField($model,'matter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter1'); ?>
		<?php echo $form->textField($model,'matter1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter2'); ?>
		<?php echo $form->textField($model,'matter2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter3'); ?>
		<?php echo $form->textField($model,'matter3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter4'); ?>
		<?php echo $form->textField($model,'matter4'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter5'); ?>
		<?php echo $form->textField($model,'matter5'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter6'); ?>
		<?php echo $form->textField($model,'matter6'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter7'); ?>
		<?php echo $form->textField($model,'matter7'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter8'); ?>
		<?php echo $form->textField($model,'matter8'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'matter9'); ?>
		<?php echo $form->textField($model,'matter9'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
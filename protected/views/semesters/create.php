<?php
/* @var $this SemestersController */
/* @var $model Semesters */

$this->breadcrumbs=array(
    Yii::t('app','Semesters')=>array('index'),
    Yii::t('app','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Semesters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage Semesters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Create Semesters'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
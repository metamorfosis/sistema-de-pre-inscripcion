<?php
/* @var $this SemestersController */
/* @var $model Semesters */

$this->breadcrumbs=array(
    Yii::t('app','Semesters')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('app','List Semesters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Semesters'), 'url'=>array('create')),
	array('label'=>Yii::t('app','Update Semesters'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete Semesters'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Semesters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View').' '.$model->name; ?></h1>
<br>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
            'name'=>'career_id',
            'value'=>$model->career->name,
        ),
        array(
            'name'=>'pensum_id',
            'value'=>$model->pensum->year,
        ),
        array(
            'name'=>'matter',
            'value'=>$model->matter0->name,
        ),
        array(
            'name'=>'matter1',
            'value'=>$model->matter10->name,
        ),
        array(
            'name'=>'matter2',
            'value'=>$model->matter20->name,
        ),
        array(
            'name'=>'matter3',
            'value'=>$model->matter30->name,
        ),
        array(
            'name'=>'matter4',
            'value'=>$model->matter40->name,
        ),
        array(
            'name'=>'matter5',
            'value'=>$model->matter50->name,
        ),
        array(
            'name'=>'matter6',
            'value'=>$model->matter60->name,
        ),
        array(
            'name'=>'matter7',
            'value'=>$model->matter70->name,
        ),
        array(
            'name'=>'matter8',
            'value'=>$model->matter80->name,
        ),
        array(
            'name'=>'matter9',
            'value'=>$model->matter90->name,
        ),
	),
)); ?>

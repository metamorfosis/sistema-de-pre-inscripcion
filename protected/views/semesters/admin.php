<?php
/* @var $this SemestersController */
/* @var $model Semesters */

$this->breadcrumbs=array(
    Yii::t('app','Semesters')=>array('index'),
    Yii::t('app','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Semesters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Semesters'), 'url'=>array('create')),
);

?>

<h1><?php echo Yii::t('app','Manage Semesters'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'semesters-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'template'=>"{items}",
	'columns'=>array(
		'id',
		'name',
        array(
            'name'=>'career_id',
            'value'=>'$data->career->name'
        ),
        array(
            'name'=>'pensum_id',
            'value'=>'$data->pensum->year'
        ),
        array(
            'name'=>'matter',
            'value'=>'$data->matter0->name'
        ),
        array(
            'name'=>'matter1',
            'value'=>'$data->matter10->name'
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

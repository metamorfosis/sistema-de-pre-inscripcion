<?php
/* @var $this SemestersController */
/* @var $model Semesters */

$this->breadcrumbs=array(
    Yii::t('app','Semesters')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
    Yii::t('app','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app','List Semesters'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Create Semesters'), 'url'=>array('create')),
	array('label'=>Yii::t('app','View Semesters'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage Semesters'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update Semesters').' #'. $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
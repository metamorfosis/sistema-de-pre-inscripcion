<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Sistema Automatizado de Registro y Control de Horario',
	'language'=>'es',
	'sourceLanguage'=>'en',
	'charset'=>'utf-8',
	'theme'=>'classic',

	// preloading 'log' component
	'preload'=>array('log'),

	// path aliases
	'aliases' => array(
		'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'),
		'vendor.twbs.bootstrap.dist' => realpath(__DIR__ . '/../extensions/bootstrap'),
		'yiistrap' => realpath(__DIR__ . '/../extensions/yiistrap'),
		'vendor.twbs.yiistrap.dist' => realpath(__DIR__ . '/../extensions/yiistrap'),
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
    	'application.controllers.*',
		'application.components.*',
		'bootstrap.behaviors.*',
	    'bootstrap.components.*',
	    'bootstrap.form.*',
	    'bootstrap.helpers.*',
	    'bootstrap.widgets.*',
	    'yiistrap.behaviors.*',
	    'yiistrap.components.*',
	    'yiistrap.form.*',
	    'yiistrap.helpers.*',
	    'yiistrap.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(

		'bootstrap' => array(
            'class' => '\TbApi',
        ),

        'yiistrap' => array(
            'class' => '\TbApi',
        ),

		'coreMessages'=>array(
			'basePath'=>'protected/messages',
		),

		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>FALSE,
			'urlSuffix'=>'.html',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, trace',
				),
				// uncomment the following to show log messages on web pages
                /*array(
                    'class'=>'CWebLogRoute',
                ),
                array(
                    'class' => 'ext.phpconsole.PhpConsoleLogRoute',
                    'phpConsolePathAlias' => 'application.vendor.PhpConsole.src.PhpConsole',
                    Default options:
                    'isEnabled' => true,
                    'handleErrors' => true,
                    'handleExceptions' => true,
                    'sourcesBasePath' => $_SERVER['DOCUMENT_ROOT'],
                    'phpConsolePathAlias' => 'application.vendors.PhpConsole.src.PhpConsole',
                    'registerHelper' => true,
                    'serverEncoding' => null,
                    'headersLimit' => null,
                    'password' => null,
                    'enableSslOnlyMode' => false,
                    'ipMasks' => array(),
                    'dumperLevelLimit' => 5,
                    'dumperItemsCountLimit' => 100,
                    'dumperItemSizeLimit' => 5000,
                    'dumperDumpSizeLimit' => 500000,
                    'dumperDetectCallbacks' => true,
                    'detectDumpTraceAndSource' => true,
                    'isEvalEnabled' => false,

                )*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);

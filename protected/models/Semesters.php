<?php

/**
 * This is the model class for table "semesters".
 *
 * The followings are the available columns in table 'semesters':
 * @property string $id
 * @property string $name
 * @property string $career_id
 * @property string $pensum_id
 * @property string $matter
 * @property string $matter1
 * @property string $matter2
 * @property string $matter3
 * @property string $matter4
 * @property string $matter5
 * @property string $matter6
 * @property string $matter7
 * @property string $matter8
 * @property string $matter9
 *
 * The followings are the available model relations:
 * @property Careers $career
 * @property Pensums $pensum
 * @property Matters $matter0
 * @property Matters $matter10
 * @property Matters $matter20
 * @property Matters $matter30
 * @property Matters $matter40
 * @property Matters $matter50
 * @property Matters $matter60
 * @property Matters $matter70
 * @property Matters $matter80
 * @property Matters $matter90
 * @property Schedules[] $schedules
 */
class Semesters extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'semesters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, career_id, pensum_id, matter', 'required'),
			array('name', 'length', 'max'=>255),
			array('matter1, matter2, matter3, matter4, matter5, matter6, matter7, matter8, matter9', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, career_id, pensum_id, matter, matter1, matter2, matter3, matter4, matter5, matter6, matter7, matter8, matter9', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'career' => array(self::BELONGS_TO, 'Careers', 'career_id'),
			'pensum' => array(self::BELONGS_TO, 'Pensums', 'pensum_id'),
			'matter0' => array(self::BELONGS_TO, 'Matters', 'matter'),
			'matter10' => array(self::BELONGS_TO, 'Matters', 'matter1'),
			'matter20' => array(self::BELONGS_TO, 'Matters', 'matter2'),
			'matter30' => array(self::BELONGS_TO, 'Matters', 'matter3'),
			'matter40' => array(self::BELONGS_TO, 'Matters', 'matter4'),
			'matter50' => array(self::BELONGS_TO, 'Matters', 'matter5'),
			'matter60' => array(self::BELONGS_TO, 'Matters', 'matter6'),
			'matter70' => array(self::BELONGS_TO, 'Matters', 'matter7'),
			'matter80' => array(self::BELONGS_TO, 'Matters', 'matter8'),
			'matter90' => array(self::BELONGS_TO, 'Matters', 'matter9'),
			'schedules' => array(self::HAS_MANY, 'Schedules', 'semester_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
            'name' => 'Nombre',
            'career_id' => 'Carrera',
            'pensum_id' => 'Pensum',
            'matter' => 'Materia',
            'matter1' => 'Materia',
            'matter2' => 'Materia',
            'matter3' => 'Materia',
            'matter4' => 'Materia',
            'matter5' => 'Materia',
            'matter6' => 'Materia',
            'matter7' => 'Materia',
            'matter8' => 'Materia',
            'matter9' => 'Materia',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('career_id',$this->career_id,true);
		$criteria->compare('pensum_id',$this->pensum_id,true);
		$criteria->compare('matter',$this->matter,true);
		$criteria->compare('matter1',$this->matter1,true);
		$criteria->compare('matter2',$this->matter2,true);
		$criteria->compare('matter3',$this->matter3,true);
		$criteria->compare('matter4',$this->matter4,true);
		$criteria->compare('matter5',$this->matter5,true);
		$criteria->compare('matter6',$this->matter6,true);
		$criteria->compare('matter7',$this->matter7,true);
		$criteria->compare('matter8',$this->matter8,true);
		$criteria->compare('matter9',$this->matter9,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Semesters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

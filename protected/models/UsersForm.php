<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property integer $ci
 * @property string $name
 * @property string $last_name
 * @property string $birthdate
 * @property string $address
 * @property string $email
 * @property string $movil
 * @property string $type
 * @property string $username
 * @property string $password
 * @property integer $status_id
 *
 * The followings are the available model relations:
 * @property Availability[] $availabilities
 * @property Pendiente[] $pendientes
 * @property Preinscription[] $preinscriptions
 * @property Studentslist[] $studentslists
 * @property Status $status
 */
class UsersForm extends CFormModel
{

    public $ci;
    public $name;
    public $last_name;
    public $birthdate;
    public $address;
    public $email;
    public $movil;
    public $type;
    public $username;
    public $accessToken;
    public $auth_key;
    public $password_hash;
    public $created_at;
    public $updated_at;
    public $status_id;

    /*public function tableName()
    {
        return 'users';
    }*/

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ci, name, last_name, address, email, movil, type', 'required'),
			array('ci, status_id', 'numerical', 'integerOnly'=>true),
			array('name, last_name, address, email, movil, type, username, password_hash', 'length', 'max'=>255),
			array('birthdate', 'safe'),
			array('ci', 'validateCI'),
			array('email', 'email'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ci' => 'Cédula',
			'name' => 'Nombre',
			'last_name' => 'Apellido',
			'birthdate' => 'Fecha de Nacimiento',
			'address' => 'Dirección',
			'email' => 'Correo',
			'movil' => 'Movil',
			'type' => 'Tipo de Usuario',
			'username' => 'Nombre de Usuario',
			'password_hash' => 'Contraseña',
			'status_id' => 'Estatus',
		);
	}

    /*public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }*/

	/** Agragadas */
	public function getMenuCareer()
	{
		$career=Careers::model()->findAll();
		return CHtml::listData($career,"id","name");
	}

	public function getMenuPensum()
	{
		$career=Pensums::model()->findAll();
		return CHtml::listData($career,"id","name");
	}

	public function getMenuAsing($type)
	{
		$role=Yii::app()->authManager->getAuthItems();
		switch ($type) {
			case '1':
				$roles = CHtml::listData($role,"name","name");
				unset($roles['Admin']);
				unset($roles['Coordinador']);
				break;

			default:
				$roles = CHtml::listData($role,"name","name");
				break;
		}

		return $roles;
	}

	public function validateCI($attributes, $params){
		//Consultando si la cedula esta en la tabla
		$CI=Users::model()->find('ci=:ci',array(':ci'=>$this->ci));
//        $cd=intval($this->ci);
		if($CI != NULL){
            if ($this->ci == $CI->ci) {
                $this->addError('ci', 'La cedula que ingresó ya esta cargada en sistema, para mayor información haz click '.'<b>'.
                    CHtml::link('Aquí', array('view', 'id'=>$CI->id),
                        array('class'=>'btn btn-success btn-mini')).'</b>');
            }
        }
        return true;
	}
}

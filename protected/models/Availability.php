<?php

/**
 * This is the model class for table "availability".
 *
 * The followings are the available columns in table 'availability':
 * @property string $id
 * @property string $days
 * @property string $hour_start
 * @property string $hour_end
 * @property string $matter_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Matters $matter
 * @property Entities $hourStart
 * @property Entities $hourEnd
 */
class Availability extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'availability';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('days', 'required'),
			array('days', 'length', 'max'=>255),
			array('hour_start, hour_end, matter_id, user_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, days, hour_start, hour_end, matter_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'matter' => array(self::BELONGS_TO, 'Matters', 'matter_id'),
			'hourStart' => array(self::BELONGS_TO, 'Entities', 'hour_start'),
			'hourEnd' => array(self::BELONGS_TO, 'Entities', 'hour_end'),
			'day' => array(self::BELONGS_TO, 'Entities', 'days'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'days' => 'Dia',
			'hour_start' => 'Hora Inicio',
			'hour_end' => 'Hora Final',
			'matter_id' => 'Materia',
			'user_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('days',$this->days,true);
		$criteria->compare('hour_start',$this->hour_start,true);
		$criteria->compare('hour_end',$this->hour_end,true);
		$criteria->compare('matter_id',$this->matter_id,true);
		$criteria->compare('user_id',$this->user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Availability the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDays()
    {
        // Consultando Dias de la Semana.
        $days = Entities::model()->findAll('description=:description', array(':description' => 'Days'));
        return CHtml::listData($days,"id","name");
    }


    public function getMenuHours()
    {
        $hours = Entities::model()->findAll('description=:description', array(':description' => 'block'));
        return CHtml::listData($hours,"id","name");
    }
}

<?php

/**
 * This is the model class for table "scheduleDetails".
 *
 * The followings are the available columns in table 'scheduleDetails':
 * @property string $id
 * @property string $schedule_id
 * @property integer $day
 * @property string $block1
 * @property string $block2
 * @property string $block3
 * @property string $block4
 * @property string $block5
 * @property string $block6
 * @property string $block7
 * @property string $block8
 * @property string $block9
 * @property string $block10
 * @property string $block11
 * @property string $block12
 * @property string $block13
 * @property string $user1_id
 * @property string $user2_id
 * @property string $user3_id
 * @property string $user4_id
 * @property string $user5_id
 * @property string $user6_id
 * @property string $user7_id
 * @property string $user8_id
 * @property string $user9_id
 * @property string $user10_id
 * @property string $user11_id
 * @property string $user12_id
 * @property string $user13_id
 *
 * The followings are the available model relations:
 * @property Schedules $schedule
 * @property Matters $block30
 * @property Matters $block40
 * @property Matters $block50
 * @property Matters $block60
 * @property Matters $block70
 * @property Matters $block80
 * @property Matters $block90
 * @property Matters $block100
 * @property Matters $block110
 * @property Matters $block120
 * @property Matters $block130
 * @property Users $user1
 * @property Users $user2
 * @property Users $user3
 * @property Users $user4
 * @property Users $user5
 * @property Users $user6
 * @property Users $user7
 * @property Users $user8
 * @property Users $user9
 * @property Users $user10
 * @property Users $user11
 * @property Users $user12
 * @property Users $user13
 * @property Entities $day0
 * @property Matters $block14
 * @property Matters $block20
 */
class ScheduleDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scheduleDetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('schedule_id, day, block1, block2, block3, block4, block5, block6, block7, block8, block9, block10, block11, block12, block13, user1_id, user2_id, user3_id, user4_id, user5_id, user6_id, user7_id, user8_id, user9_id, user10_id, user11_id, user12_id, user13_id', 'required'),
			array('day', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, schedule_id, day, block1, block2, block3, block4, block5, block6, block7, block8, block9, block10, block11, block12, block13, user1_id, user2_id, user3_id, user4_id, user5_id, user6_id, user7_id, user8_id, user9_id, user10_id, user11_id, user12_id, user13_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'schedule' => array(self::BELONGS_TO, 'Schedules', 'schedule_id'),
			'block30' => array(self::BELONGS_TO, 'Matters', 'block3'),
			'block40' => array(self::BELONGS_TO, 'Matters', 'block4'),
			'block50' => array(self::BELONGS_TO, 'Matters', 'block5'),
			'block60' => array(self::BELONGS_TO, 'Matters', 'block6'),
			'block70' => array(self::BELONGS_TO, 'Matters', 'block7'),
			'block80' => array(self::BELONGS_TO, 'Matters', 'block8'),
			'block90' => array(self::BELONGS_TO, 'Matters', 'block9'),
			'block100' => array(self::BELONGS_TO, 'Matters', 'block10'),
			'block110' => array(self::BELONGS_TO, 'Matters', 'block11'),
			'block120' => array(self::BELONGS_TO, 'Matters', 'block12'),
			'block130' => array(self::BELONGS_TO, 'Matters', 'block13'),
			'user1' => array(self::BELONGS_TO, 'Users', 'user1_id'),
			'user2' => array(self::BELONGS_TO, 'Users', 'user2_id'),
			'user3' => array(self::BELONGS_TO, 'Users', 'user3_id'),
			'user4' => array(self::BELONGS_TO, 'Users', 'user4_id'),
			'user5' => array(self::BELONGS_TO, 'Users', 'user5_id'),
			'user6' => array(self::BELONGS_TO, 'Users', 'user6_id'),
			'user7' => array(self::BELONGS_TO, 'Users', 'user7_id'),
			'user8' => array(self::BELONGS_TO, 'Users', 'user8_id'),
			'user9' => array(self::BELONGS_TO, 'Users', 'user9_id'),
			'user10' => array(self::BELONGS_TO, 'Users', 'user10_id'),
			'user11' => array(self::BELONGS_TO, 'Users', 'user11_id'),
			'user12' => array(self::BELONGS_TO, 'Users', 'user12_id'),
			'user13' => array(self::BELONGS_TO, 'Users', 'user13_id'),
			'day0' => array(self::BELONGS_TO, 'Entities', 'day'),
			'block14' => array(self::BELONGS_TO, 'Matters', 'block1'),
			'block20' => array(self::BELONGS_TO, 'Matters', 'block2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
            'id' => 'ID',
            'schedule_id' => 'Horario',
            'day' => 'Dia',
            'block1' => '7:30 - 8:15',
            'block2' => '8:15 - 9:00',
            'block3' => '9:00 - 9:45',
            'block4' => '9:45 - 10:30',
            'block5' => '10:30 - 11:15',
            'block6' => '11:15 - 12:00',
            'block7' => '12:00 - 1:00',
            'block8' => '1:00 - 1:45',
            'block9' => '1:45 - 2:30',
            'block10' => '2:30 - 3:15',
            'block11' => '3:15 - 4:00',
            'block12' => '4:00 - 4:45',
            'block13' => '4:45 - 5:30',
            'user1_id' => 'Profesor',
            'user2_id' => 'Profesor',
            'user3_id' => 'Profesor',
            'user4_id' => 'Profesor',
            'user5_id' => 'Profesor',
            'user6_id' => 'Profesor',
            'user7_id' => 'Profesor',
            'user8_id' => 'Profesor',
            'user9_id' => 'Profesor',
            'user10_id' => 'Profesor',
            'user11_id' => 'Profesor',
            'user12_id' => 'Profesor',
            'user13_id' => 'Profesor',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('schedule_id',$this->schedule_id,true);
		$criteria->compare('day',$this->day);
		$criteria->compare('block1',$this->block1,true);
		$criteria->compare('block2',$this->block2,true);
		$criteria->compare('block3',$this->block3,true);
		$criteria->compare('block4',$this->block4,true);
		$criteria->compare('block5',$this->block5,true);
		$criteria->compare('block6',$this->block6,true);
		$criteria->compare('block7',$this->block7,true);
		$criteria->compare('block8',$this->block8,true);
		$criteria->compare('block9',$this->block9,true);
		$criteria->compare('block10',$this->block10,true);
		$criteria->compare('block11',$this->block11,true);
		$criteria->compare('block12',$this->block12,true);
		$criteria->compare('block13',$this->block13,true);
		$criteria->compare('user1_id',$this->user1_id,true);
		$criteria->compare('user2_id',$this->user2_id,true);
		$criteria->compare('user3_id',$this->user3_id,true);
		$criteria->compare('user4_id',$this->user4_id,true);
		$criteria->compare('user5_id',$this->user5_id,true);
		$criteria->compare('user6_id',$this->user6_id,true);
		$criteria->compare('user7_id',$this->user7_id,true);
		$criteria->compare('user8_id',$this->user8_id,true);
		$criteria->compare('user9_id',$this->user9_id,true);
		$criteria->compare('user10_id',$this->user10_id,true);
		$criteria->compare('user11_id',$this->user11_id,true);
		$criteria->compare('user12_id',$this->user12_id,true);
		$criteria->compare('user13_id',$this->user13_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScheduleDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

class ValidatePassword extends CFormModel
{
	public $id;
	public $new;
	public $new_repeat;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array(
				'new, new_repeat',
				'required',
				'message' => 'Este campo es requerido',
			),
			array(
				'new, new_repeat',
				'match',
				'pattern' => '/^[a-zA-Z0-9ñÑ\_@&$%*.+-]+$/i',
				'message' => 'Error: Sólo letras, números y carácteres especiales (.+-_). Se distingue Mayúsculas de Minúsculas.',
			),
			array(
				'new_repeat',
				'compare',
				'compareAttribute' => 'new',
				'message' => 'Error: La contraseña no coincide',
			),
			array('new', 'validateNew'),
			/*
			/[A-Za-zñÑáéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñäëïöüÄËÏÖÜ\s\t]/
			*/
		);
	}

	public function validateNew($attributes, $params){
		//Consultando si el username esta en la tabla
		$user=Users::model()->findAll('id=:id',array(':id'=>$this->id));
		foreach ($user as $fila) {
			if (md5($this->new) === $fila['password_hash']) {
				$this->addError('new', 'La nueva contraseña no puede ser igual a la contraseña actual');
				break;
			}
		}
	}
}

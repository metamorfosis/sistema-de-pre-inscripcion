<?php

/**
 * ListForm class.
 */
class ListForm extends CFormModel
{
    public $userNumber;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userNumber', 'required'),
            array('userNumber', 'numerical', 'integerOnly' => true),
            array('userNumber', 'getUserNumber'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'userNumber' => 'Numero de Estudiantes',
        );
    }

    public function getUsersList()
    {
        $list = Studentslist::model()->findAll();
        return $list;
        //return CHtml::listData($list,"id","user_id");
    }

    public function getCountUsers()
    {
        // Consultando cantidad de usurios estudiantes
        $users = Users::model()->count('type=:type', array(':type' => 'Estudiante'));
        return $users;
    }

    public function getUserNumber()
    {
        // devolviendo cantidad de alumnos
        if ($this->userNumber > 0) {
            return $this->userNumber;
        } else {
            Yii::app()->user->setFlash('error', 'La cantidad de usuario no puede ser 0');
        }
    }
}

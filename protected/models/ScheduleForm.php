<?php

/**
 * This is the model class for table "schedule_details".
 *
 * The followings are the available columns in table 'schedule_details':
 * @property integer $id
 * @property integer $schedule_id
 * @property integer $day
 * @property integer $block1
 * @property integer $block2
 * @property integer $block3
 * @property integer $block4
 * @property integer $block5
 * @property integer $block6
 * @property integer $block7
 * @property integer $block8
 * @property integer $block9
 * @property integer $block10
 * @property integer $block11
 * @property integer $block12
 * @property integer $block13
 * @property integer $user1_id
 * @property integer $user2_id
 * @property integer $user3_id
 * @property integer $user4_id
 * @property integer $user5_id
 * @property integer $user6_id
 * @property integer $user7_id
 * @property integer $user8_id
 * @property integer $user9_id
 * @property integer $user10_id
 * @property integer $user11_id
 * @property integer $user12_id
 * @property integer $user13_id
 *
 * The followings are the available model relations:
 * @property Matters $block14
 * @property Matters $block100
 * @property Matters $block110
 * @property Matters $block120
 * @property Matters $block130
 * @property Matters $block20
 * @property Matters $block30
 * @property Matters $block40
 * @property Matters $block50
 * @property Matters $block60
 * @property Matters $block70
 * @property Matters $block80
 * @property Matters $block90
 * @property Schedules $schedule
 * @property Users $user1
 * @property Users $user10
 * @property Users $user11
 * @property Users $user12
 * @property Users $user13
 * @property Users $user2
 * @property Users $user3
 * @property Users $user4
 * @property Users $user5
 * @property Users $user6
 * @property Users $user7
 * @property Users $user8
 * @property Users $user9
 */
class ScheduleForm extends CActiveRecord
{
    /*public $id;
    public $schedule_id;
    public $day;
    public $block1;
    public $block2;
    public $block3;
    public $block4;
    public $block5;
    public $block6;
    public $block7;
    public $block8;
    public $block9;
    public $block10;
    public $block11;
    public $block12;
    public $block13;
    public $user1_id;
    public $user2_id;
    public $user3_id;
    public $user4_id;
    public $user5_id;
    public $user6_id;
    public $user7_id;
    public $user8_id;
    public $user9_id;
    public $user10_id;
    public $user11_id;
    public $user12_id;
    public $user13_id;*/

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'scheduleDetails';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('schedule_id, day', 'required'),
			array('schedule_id, day, block1, block2, block3, block4, block5, block6, block7, block8, block9, block10, block11, block12, block13, user1_id, user2_id, user3_id, user4_id, user5_id, user6_id, user7_id, user8_id, user9_id, user10_id, user11_id, user12_id, user13_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, schedule_id, day, block1, block2, block3, block4, block5, block6, block7, block8, block9, block10, block11, block12, block13, user1_id, user2_id, user3_id, user4_id, user5_id, user6_id, user7_id, user8_id, user9_id, user10_id, user11_id, user12_id, user13_id', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'schedule' => array(self::BELONGS_TO, 'Schedules', 'schedule_id'),
            'block30' => array(self::BELONGS_TO, 'Matters', 'block3'),
            'block40' => array(self::BELONGS_TO, 'Matters', 'block4'),
            'block50' => array(self::BELONGS_TO, 'Matters', 'block5'),
            'block60' => array(self::BELONGS_TO, 'Matters', 'block6'),
            'block70' => array(self::BELONGS_TO, 'Matters', 'block7'),
            'block80' => array(self::BELONGS_TO, 'Matters', 'block8'),
            'block90' => array(self::BELONGS_TO, 'Matters', 'block9'),
            'block100' => array(self::BELONGS_TO, 'Matters', 'block10'),
            'block110' => array(self::BELONGS_TO, 'Matters', 'block11'),
            'block120' => array(self::BELONGS_TO, 'Matters', 'block12'),
            'block130' => array(self::BELONGS_TO, 'Matters', 'block13'),
            'user1' => array(self::BELONGS_TO, 'Users', 'user1_id'),
            'user2' => array(self::BELONGS_TO, 'Users', 'user2_id'),
            'user3' => array(self::BELONGS_TO, 'Users', 'user3_id'),
            'user4' => array(self::BELONGS_TO, 'Users', 'user4_id'),
            'user5' => array(self::BELONGS_TO, 'Users', 'user5_id'),
            'user6' => array(self::BELONGS_TO, 'Users', 'user6_id'),
            'user7' => array(self::BELONGS_TO, 'Users', 'user7_id'),
            'user8' => array(self::BELONGS_TO, 'Users', 'user8_id'),
            'user9' => array(self::BELONGS_TO, 'Users', 'user9_id'),
            'user10' => array(self::BELONGS_TO, 'Users', 'user10_id'),
            'user11' => array(self::BELONGS_TO, 'Users', 'user11_id'),
            'user12' => array(self::BELONGS_TO, 'Users', 'user12_id'),
            'user13' => array(self::BELONGS_TO, 'Users', 'user13_id'),
            'day0' => array(self::BELONGS_TO, 'Entities', 'day'),
            'block14' => array(self::BELONGS_TO, 'Matters', 'block1'),
            'block20' => array(self::BELONGS_TO, 'Matters', 'block2'),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'schedule_id' => 'Horario',
			'day' => 'Dia',
			'block1' => '7:30 - 8:15',
			'block2' => '8:15 - 9:00',
			'block3' => '9:00 - 9:45',
			'block4' => '9:45 - 10:30',
			'block5' => '10:30 - 11:15',
			'block6' => '11:15 - 12:00',
			'block7' => '12:00 - 1:00',
			'block8' => '1:00 - 1:45',
			'block9' => '1:45 - 2:30',
			'block10' => '2:30 - 3:15',
			'block11' => '3:15 - 4:00',
			'block12' => '4:00 - 4:45',
			'block13' => '4:45 - 5:30',
			'user1_id' => 'Profesor',
			'user2_id' => 'Profesor',
			'user3_id' => 'Profesor',
			'user4_id' => 'Profesor',
			'user5_id' => 'Profesor',
			'user6_id' => 'Profesor',
			'user7_id' => 'Profesor',
			'user8_id' => 'Profesor',
			'user9_id' => 'Profesor',
			'user10_id' => 'Profesor',
			'user11_id' => 'Profesor',
			'user12_id' => 'Profesor',
			'user13_id' => 'Profesor',
		);
	}

    public function getDays()
    {
        // Consultando Dias de la Semana.
        $days = Entities::model()->findAll('description=:description', array(':description' => 'Days'));
        return CHtml::listData($days,"id","name");
    }

    public function getTeachers()
    {
        // Consultando users para extraer los profesores.

        $criteria = new CDbCriteria;
        $criteria->addCondition('type=:type');
        //$criteria->addCondition('matter_id=:matter_id','AND');
        //$criteria->params = array(':type' => 'Profesor', 'matter_id'=>$matter);
        $criteria->params = array(':type' => 'Profesor');
        $criteria->order = 'last_name';
        $criteria->limit = 30;
        $data = Users::model()->findAll($criteria);

        if (!empty($data))
        {
            $users = array();
            foreach ($data as $item) {
                $users[$item->id] = $item->matter->cod.' - '.$item->last_name.' '.$item->name;
            }
        }
        return $users;
    }
}

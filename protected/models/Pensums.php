<?php

/**
 * This is the model class for table "pensums".
 *
 * The followings are the available columns in table 'pensums':
 * @property string $id
 * @property integer $year
 * @property string $turn
 * @property string $career_id
 *
 * The followings are the available model relations:
 * @property Users[] $users
 * @property Careers $career
 * @property Entities $turn0
 * @property Semesters[] $semesters
 */
class Pensums extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pensums';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('year, turn, career_id', 'required'),
			array('year', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, year, turn, career_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'Users', 'pensum_id'),
			'career' => array(self::BELONGS_TO, 'Careers', 'career_id'),
			'turn0' => array(self::BELONGS_TO, 'Entities', 'turn'),
			'semesters' => array(self::HAS_MANY, 'Semesters', 'pensum_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'year' => 'Año',
			'turn' => 'Turno',
			'career_id' => 'Carrera',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('turn',$this->turn,true);
		$criteria->compare('career_id',$this->career_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pensums the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // Agregadas
    public function getMenuTurn()
    {
        $career=Entities::model()->findAll('description=:description',array(':description'=>'Turn'));
        return CHtml::listData($career,"id","name");
    }

    public function getMenuCareers()
    {
        $career=Careers::model()->findAll();
        return CHtml::listData($career,"id","name");
    }
}

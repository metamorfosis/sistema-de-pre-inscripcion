<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property integer $ci
 * @property string $name
 * @property string $last_name
 * @property string $birthday
 * @property string $address
 * @property string $phone
 * @property string $type
 * @property string $matter_id
 * @property string $pensum_id
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property string $accessToken
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Availability[] $availabilities
 * @property Matters $matter
 * @property Pensums $pensum
 * @property ScheduleDetails[] $scheduleDetails
 * @property ScheduleDetails[] $scheduleDetails1
 * @property ScheduleDetails[] $scheduleDetails2
 * @property ScheduleDetails[] $scheduleDetails3
 * @property ScheduleDetails[] $scheduleDetails4
 * @property ScheduleDetails[] $scheduleDetails5
 * @property ScheduleDetails[] $scheduleDetails6
 * @property ScheduleDetails[] $scheduleDetails7
 * @property ScheduleDetails[] $scheduleDetails8
 * @property ScheduleDetails[] $scheduleDetails9
 * @property ScheduleDetails[] $scheduleDetails10
 * @property ScheduleDetails[] $scheduleDetails11
 * @property ScheduleDetails[] $scheduleDetails12
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		/* *
		    NOTE: you should only define rules for those attributes that
		    will receive user inputs.
		*/
		return array(
			array('name, last_name, birthday, address, phone, type, email', 'required'),
			array('ci', 'numerical', 'integerOnly'=>true),
			array('name, last_name, username, email, password_hash, password_reset_token', 'length', 'max'=>255),
			array('address', 'length', 'max'=>1500),
			array('phone, type', 'length', 'max'=>15),
			array('auth_key, accessToken', 'length', 'max'=>32),
			array('matter_id, pensum_id, status_id, birthday', 'safe'),
            array('ci', 'validateCI'),
            array('email', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ci, name, last_name, birthday, address, phone, type, matter_id, pensum_id, username, email, auth_key, accessToken, password_hash, password_reset_token, status_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'availabilities' => array(self::HAS_MANY, 'Availability', 'user_id'),
			'matter' => array(self::BELONGS_TO, 'Matters', 'matter_id'),
			'pensum' => array(self::BELONGS_TO, 'Pensums', 'pensum_id'),
			'statusu' => array(self::BELONGS_TO, 'Status', 'status_id'),
			'scheduleDetails' => array(self::HAS_MANY, 'ScheduleDetails', 'user1_id'),
			'scheduleDetails1' => array(self::HAS_MANY, 'ScheduleDetails', 'user2_id'),
			'scheduleDetails2' => array(self::HAS_MANY, 'ScheduleDetails', 'user3_id'),
			'scheduleDetails3' => array(self::HAS_MANY, 'ScheduleDetails', 'user4_id'),
			'scheduleDetails4' => array(self::HAS_MANY, 'ScheduleDetails', 'user5_id'),
			'scheduleDetails5' => array(self::HAS_MANY, 'ScheduleDetails', 'user6_id'),
			'scheduleDetails6' => array(self::HAS_MANY, 'ScheduleDetails', 'user7_id'),
			'scheduleDetails7' => array(self::HAS_MANY, 'ScheduleDetails', 'user8_id'),
			'scheduleDetails8' => array(self::HAS_MANY, 'ScheduleDetails', 'user9_id'),
			'scheduleDetails9' => array(self::HAS_MANY, 'ScheduleDetails', 'user10_id'),
			'scheduleDetails10' => array(self::HAS_MANY, 'ScheduleDetails', 'user11_id'),
			'scheduleDetails11' => array(self::HAS_MANY, 'ScheduleDetails', 'user12_id'),
			'scheduleDetails12' => array(self::HAS_MANY, 'ScheduleDetails', 'user13_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ci' => 'CI',
            'name' => 'Nombre',
            'last_name' => 'Apellido',
            'birthday' => 'Fecha de Nacimiento',
            'address' => 'Dirección',
            'phone' => 'Movil',
            'type' => 'Tipo de Usuario',
            'matter_id' => 'Materia',
            'pensum_id' => 'Pensum',
            'username' => 'Nombre de Usuario',
            'email' => 'Correo',
            'auth_key' => 'Llave de Autenticación',
            'accessToken' => 'Token de Acceso',
            'password_hash' => 'Contraseña',
            'password_reset_token' => 'Password Reset Token',
            'status_id' => 'Estatus',
            'created_at' => 'Creado el',
            'updated_at' => 'Actualizado el',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ci',$this->ci);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('matter_id',$this->matter_id,true);
		$criteria->compare('pensum_id',$this->pensum_id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('auth_key',$this->auth_key,true);
		$criteria->compare('accessToken',$this->accessToken,true);
		$criteria->compare('password_hash',$this->password_hash,true);
		$criteria->compare('password_reset_token',$this->password_reset_token,true);
		$criteria->compare('status_id',$this->status_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /** Agragadas */
    public function getMenuCareer()
    {
        $career=Careers::model()->findAll();
        return CHtml::listData($career,"id","name");
    }

    public function getMenuPensum()
    {
        $career=Pensums::model()->findAll();
        return CHtml::listData($career,"id","name");
    }

    public function getMenuAsing($type)
    {
        $role=Yii::app()->authManager->getAuthItems();
        switch ($type) {
            case '1':
                $roles = CHtml::listData($role,"name","name");
                unset($roles['Admin']);
                unset($roles['Coordinador']);
                break;

            default:
                $roles = CHtml::listData($role,"name","name");
                break;
        }

        return $roles;
    }

    public function validateCI($attributes, $params){
        if ($this->getIsNewRecord()){
            // Consultando si la cédula esta en la tabla
            $CI=Users::model()->find('ci=:ci',array(':ci'=>$this->ci));
            // $cd=intval($this->ci);
            if($CI != NULL){
                if ($this->ci == $CI->ci) {
                    $this->addError('ci', 'La cedula que ingresó ya esta cargada en sistema, para mayor información haz click '.'<b>'.
                        CHtml::link('Aquí', array('view', 'id'=>$CI->id),
                            array('class'=>'btn btn-success btn-mini')).'</b>');
                }
            }
        }
        return true;
    }

}

<?php

/**
 * This is the model class for table "matters".
 *
 * The followings are the available columns in table 'matters':
 * @property string $id
 * @property string $cod
 * @property string $name
 * @property string $priority1
 * @property string $priority2
 * @property integer $unitsCredit
 * @property string $hoursLab
 * @property string $hoursTheory
 * @property string $hoursPractice
 *
 * The followings are the available model relations:
 * @property Semesters[] $semesters
 * @property Semesters[] $semesters1
 * @property Semesters[] $semesters2
 * @property Semesters[] $semesters3
 * @property Semesters[] $semesters4
 * @property Semesters[] $semesters5
 * @property Semesters[] $semesters6
 * @property Semesters[] $semesters7
 * @property Semesters[] $semesters8
 * @property Semesters[] $semesters9
 * @property Availability[] $availabilities
 * @property Users[] $users
 * @property ScheduleDetails[] $scheduleDetails
 * @property ScheduleDetails[] $scheduleDetails1
 * @property ScheduleDetails[] $scheduleDetails2
 * @property ScheduleDetails[] $scheduleDetails3
 * @property ScheduleDetails[] $scheduleDetails4
 * @property ScheduleDetails[] $scheduleDetails5
 * @property ScheduleDetails[] $scheduleDetails6
 * @property ScheduleDetails[] $scheduleDetails7
 * @property ScheduleDetails[] $scheduleDetails8
 * @property ScheduleDetails[] $scheduleDetails9
 * @property ScheduleDetails[] $scheduleDetails10
 * @property ScheduleDetails[] $scheduleDetails11
 * @property ScheduleDetails[] $scheduleDetails12
 */
class Matters extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod, name, priority1, unitsCredit, hoursTheory', 'required'),
			array('unitsCredit', 'numerical', 'integerOnly'=>true),
			array('cod, name, priority1, priority2, hoursLab, hoursTheory, hoursPractice', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cod, name, priority1, priority2, unitsCredit, hoursLab, hoursTheory, hoursPractice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'semesters' => array(self::HAS_MANY, 'Semesters', 'matter'),
			'semesters1' => array(self::HAS_MANY, 'Semesters', 'matter1'),
			'semesters2' => array(self::HAS_MANY, 'Semesters', 'matter2'),
			'semesters3' => array(self::HAS_MANY, 'Semesters', 'matter3'),
			'semesters4' => array(self::HAS_MANY, 'Semesters', 'matter4'),
			'semesters5' => array(self::HAS_MANY, 'Semesters', 'matter5'),
			'semesters6' => array(self::HAS_MANY, 'Semesters', 'matter6'),
			'semesters7' => array(self::HAS_MANY, 'Semesters', 'matter7'),
			'semesters8' => array(self::HAS_MANY, 'Semesters', 'matter8'),
			'semesters9' => array(self::HAS_MANY, 'Semesters', 'matter9'),
			'availabilities' => array(self::HAS_MANY, 'Availability', 'matter_id'),
			'users' => array(self::HAS_MANY, 'Users', 'matter_id'),
			'scheduleDetails' => array(self::HAS_MANY, 'ScheduleDetails', 'block1'),
			'scheduleDetails1' => array(self::HAS_MANY, 'ScheduleDetails', 'block2'),
			'scheduleDetails2' => array(self::HAS_MANY, 'ScheduleDetails', 'block3'),
			'scheduleDetails3' => array(self::HAS_MANY, 'ScheduleDetails', 'block4'),
			'scheduleDetails4' => array(self::HAS_MANY, 'ScheduleDetails', 'block5'),
			'scheduleDetails5' => array(self::HAS_MANY, 'ScheduleDetails', 'block6'),
			'scheduleDetails6' => array(self::HAS_MANY, 'ScheduleDetails', 'block7'),
			'scheduleDetails7' => array(self::HAS_MANY, 'ScheduleDetails', 'block8'),
			'scheduleDetails8' => array(self::HAS_MANY, 'ScheduleDetails', 'block9'),
			'scheduleDetails9' => array(self::HAS_MANY, 'ScheduleDetails', 'block10'),
			'scheduleDetails10' => array(self::HAS_MANY, 'ScheduleDetails', 'block11'),
			'scheduleDetails11' => array(self::HAS_MANY, 'ScheduleDetails', 'block12'),
			'scheduleDetails12' => array(self::HAS_MANY, 'ScheduleDetails', 'block13'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod' => 'Código',
            'name' => 'Nombre',
            'priority1' => 'Prelación 1',
            'priority2' => 'Prelación 2',
			'unitsCredit' => 'Unidades de Crédito',
			'hoursLab' => 'Horas Laboratorio',
			'hoursTheory' => 'Horas Teoría',
			'hoursPractice' => 'Horas Práctica',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('cod',$this->cod,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('priority1',$this->priority1,true);
		$criteria->compare('priority2',$this->priority2,true);
		$criteria->compare('unitsCredit',$this->unitsCredit);
		$criteria->compare('hoursLab',$this->hoursLab,true);
		$criteria->compare('hoursTheory',$this->hoursTheory,true);
		$criteria->compare('hoursPractice',$this->hoursPractice,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Matters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

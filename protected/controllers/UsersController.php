<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','create','update','delete','assign','changepass', 'success','autocomplete','preinscription','account','availability','autocompletematters'),
                'roles'=>array('Admin', 'Coordinador'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','update','changepass', 'success','account','availability','autocompletematters'),
                'roles'=>array('Profesor'),
            ),
            array('allow',
                'actions'=>array('update','changepass', 'success','account','preinscription'),
                'roles'=>array('Estudiante'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];

            if ($model->validate()) {
                // Asignando y Encriptando el Password con el valor de la cédula.
                $model->password_hash = md5($model->ci);
                // Asignando el username con el valor de la cédula.
                $model->username = $model->ci;
                // Seteando status a Primer Ingreso.
                $model->status_id = 3;
                // Validando Tipo de usuario para setear Pensum
                if ($model->type == 'Admin' || $model->type == 'Coordinador' || $model->type == 'Profesor'|| $model->type == 'Estudiante'){
                    $model->pensum_id = 1;
                }
                $model->auth_key = md5($model->ci);
                $model->accessToken = md5($model->ci);
                $model->created_at = new CDbExpression('NOW()');
                $model->updated_at = new CDbExpression('NOW()');
                $type = $model->type;
                if($model->save()){
                    // Obtener el id del regristro creado
                    $lastId = $model->primaryKey;
                    Yii::app()->authManager->assign($type,$lastId);
                    // Mostrando mensaje de Éxito
                    Yii::app()->user->setFlash('success','Se creo usuario con ÉXITO!');

                    //if ($type === 'Profesor'){
                    //    $this->redirect(array('availability/created','id'=>$lastId));
                    // }
                    // TODO: Si el usuario es Profesor redirigir a Availability Module "Availability/create" (Disponibilidad de materias)

                    $this->redirect(array('view','id'=>$model->id));
                }
            }
            else{
                $model->addError('ci', 'Error al enviar el Formulario');
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if ($model->type == 'Admin' || $model->type == 'Coordinador' || $model->type == 'Profesor'){
                    $model->pensum_id = 1;
                }
            if($model->save()){
                if(Yii::app()->user->visible){
                    Yii::app()->user->setFlash('success','Se realizó la actualización con ÉXITO!');
                    $this->redirect(array('view','id'=>$model->id));
                }
                else{
                    Yii::app()->user->setFlash('success','Se realizó la actualización con ÉXITO!');
                    $this->redirect(array('account','id'=>$model->id));
                }
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
            'id'=>Yii::app()->user->id
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /* Agregadas */

    public function actionAutocomplete($term)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(year)', strtolower($_GET['term']), true);
        $criteria->order = 'year';
        $criteria->limit = 30;
        $data = Pensums::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->id,
                    'value' => $item->year,
                    'label' => $item->year,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }

    public function actionAutocompleteMatters($term)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(cod)', strtolower($_GET['term']), true);
        $criteria->order = 'cod';
        $criteria->limit = 30;
        $data = Matter::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->id,
                    'value' => $item->cod.' - '.$item->name,
                    'label' => $item->cod.' - '.$item->name,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }

    public function actionAssign($id, $page)
    {
        if (Yii::app()->authManager->checkAccess($_GET["item"],$id))
        {
            Yii::app()->authManager->revoke($_GET["item"],$id);
        }
        else
        {
            Yii::app()->authManager->assign($_GET["item"],$id);
        }
        $this->redirect(array($page, "id"=>$id));
    }

    public function actionChangePass()
    {
        $model=new ValidatePassword;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'formChangepass') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['ValidatePassword']))
        {
            $model->attributes=$_POST['ValidatePassword'];
            $model->id = Yii::app()->user->id;
            if (!$model->validate()) {
                $model->addError('new_repeat', 'Error al enviar el Formulario');
            }
            else
            {
                $pass = $this->loadModel($model->id);
                $pass->password_hash = md5($model->new);
                if($pass->status_id == 3){
                    $pass->status_id = 1;
                }
                if ($pass->save(false)) {
                    Yii::app()->user->setFlash('success','Se realizó la actualización con ÉXITO!');
                    $this->redirect(array('account','id'=>Yii::app()->user->id));
                }

            }
        }

        $this->render('changepass',array(
                'model'=>$model,
                'id'=>Yii::app()->user->id,
                'user'=>$this->loadModel(Yii::app()->user->id)
            )
        );
    }

    public function actionSuccess($id)
    {
        switch ($id) {
            case '1':
                $message= "Cambio de Clave Satisfactorio";
                break;

            default:
                # code...
                break;
        }

        $this->render('success',array(
                'message'=>$message,
            )
        );
    }

    public function actionAccount($id)
    {

        $this->render('account',array(
                'model'=>$this->loadModel($id),
            )
        );
    }

    public function actionPreinscription()
    {
        $model = new Preinscription;

        $user=$this->loadModel(Yii::app()->user->id);

        //Consultando la Vista Semesterpre
        $semestre=Semesterpre::model()->findAll('Pensum=:Pensum',array(':Pensum'=>$user->pensum_id));
        $semestres=CHtml::listData($semestre, 'ID', 'Semestre');

        if(isset($_POST['Preinscription']))
        {
            $model->attributes=$_POST['Preinscription'];
            if (!$model->validate()) {
                // $model->addError('semestre_id', 'Error al enviar el Formulario');
            }
            else
            {
                $model->solicitud = new CDbExpression('NOW()');
                if($model->save())
                    $this->redirect(array('preinscription/view','id'=>$model->id));
            }
        }

        $this->render('preinscription',array(
                'model'=>$model,
                'user_id'=>$user->id,
                'pensum'=>$user->pensum_id,
                'semestres'=>$semestres,
                'user'=>$user,
            )
        );
    }

    public function actionAvailability()
    {
        $model = new Availability;

        $user=$this->loadModel(Yii::app()->user->id);

        //Crear Bloques de horas
        $horas=array(
            '1'=>'7:00 - 7:45',
            '2'=>'7:45 - 8:30',
            '3'=>'8:30 - 9:15',
            '4'=>'9:15 - 10:00',
            '5'=>'10:00 - 10:45',
            '6'=>'10:45 - 11:30',
            '7'=>'11:30 - 12:15',
            '8'=>'1:00 - 1:45',
            '9'=>'1:45 - 2:30',
            '10'=>'2:30 - 3:15',
            '11'=>'3:15 - 3:45',
            '12'=>'3:45 - 4:30',
            '13'=>'4:30 - 5:15',
        );

        $dia = array(
            '1'=>'Lunes',
            '2'=>'Martes',
            '3'=>'Miercoles',
            '4'=>'Jueves',
            '5'=>'Viernes',
            '6'=>'Sabado',
            '7'=>'Domingo',
        );
        if(isset($_POST['Availability']))
        {
            $model->attributes=$_POST['Availability'];
            $model->user_id=Yii::app()->user->id;
            if($model->save())
                $this->redirect(array('availability/view','id'=>$model->id));
        }

        $this->render('availability',array(
                'model'=>$model,
                'id'=>$user->id,
                'dia'=>$dia,
                'horas'=>$horas,
            )
        );
    }
}

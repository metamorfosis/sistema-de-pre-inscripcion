<?php

class MattersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','create','update','delete','assign','changepass', 'success','autocomplete','aclist'),
                'roles'=>array('Admin', 'Coordinador'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','update','changepass', 'success'),
                'roles'=>array('Profesor'),
            ),
            array('allow',
                'actions'=>array('update','changepass', 'success'),
                'roles'=>array('Estudiante'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Matters;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Matters']))
		{
		    //var_dump($_POST['Matters']);
			$model->attributes=$_POST['Matters'];
           // var_dump($model->attributes); die();

            if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Matters']))
		{
			$model->attributes=$_POST['Matters'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Matters');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Matters('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Matters']))
			$model->attributes=$_GET['Matters'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Matters the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Matters::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Matters $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='matters-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



    /* Agregadas */

    public function action()
    {
        return array(
            'usrlist'=>array(
                'class'=>'application.extensions.EAutoCompleteAction',
                'model'=>'Matter', //Modelo a trabajar
                'attribute'=>'cod', //Campos a mostrar en el textbox
                'id' => 'cod', //Valor a guardar en la base de datos
            ),
        );
    }

    public function actionAutocomplete($term)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(cod)', strtolower($_GET['term']), true);
        $criteria->order = 'cod';
        $criteria->limit = 30;
        $data = Matters::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->cod,
                    'value' => $item->cod,
                    'label' => $item->cod .' - '. $item->name,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }
}

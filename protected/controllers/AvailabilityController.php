<?php

class AvailabilityController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','create','update','delete','autocomplete','new'),
                'roles'=>array('Admin', 'Coordinador', 'Profesor'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionNew($ide)
	{
		$model=new Availability;
		$model->user_id = $ide;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Availability']))
		{
			$model->attributes=$_POST['Availability'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('new',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		$model=new Availability;
        $model->user_id = Yii::app()->user->id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Availability']))
		{
			$model->attributes=$_POST['Availability'];
			$model->matter_id=1;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Availability']))
		{
			$model->attributes=$_POST['Availability'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Availability');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Availability('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Availability']))
			$model->attributes=$_GET['Availability'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Availability the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Availability::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Availability $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='availability-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /* Agregadas */
    public function actionAutocomplete($term)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(cod)', strtolower($_GET['term']), true);
        $criteria->order = 'cod';
        $criteria->limit = 30;
        $data = Matters::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->id,
                    'value' => $item->cod.' '.$item->name,
                    'label' => $item->cod.' '.$item->name,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }
}

<?php

class ScheduleDetailsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','create','update','delete','autocompletematter'),
                'roles'=>array('Admin', 'Coordinador'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view','admin','update'),
                'roles'=>array('Profesor'),
            ),
            array('allow',
                'actions'=>array('index','view'),
                'roles'=>array('Estudiante'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new ScheduleForm;
        $days = $model->getDays();

        if(!isset(Yii::app()->session['day'])){
            Yii::app()->session['day'] = 1;
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ScheduleForm']))
		{
			$model->attributes=$_POST['ScheduleForm'];
			if(!isset(Yii::app()->session['schedule'])){
                Yii::app()->session['schedule'] = array();
            }

            Yii::app()->session['schedule'] += array($model->attributes);
            if(Yii::app()->session['day'] == 7){
                $scheduleSession = Yii::app()->session['schedule'];
                unset(Yii::app()->session['schedule']);
                unset(Yii::app()->session['day']);

                foreach ($scheduleSession as $item){
			        $scheduleModel = new ScheduleDetails;
                    $scheduleModel->attributes = $item;
                    if($scheduleModel->save()){
                        if($scheduleModel->day == 7){
                            $this->redirect(array('view','id'=>$scheduleModel->schedule_id));
                        }
                    }
                }
            }
            else{
                $model = new ScheduleForm;
                Yii::app()->session['day'] += 1;
            }
		}

		$this->render('create',array(
			'model'=>$model,
            'id'=>$id,
            'day'=>Yii::app()->session['day'],
            'days'=>$days,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ScheduleDetails']))
		{
			$model->attributes=$_POST['ScheduleDetails'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ScheduleDetails');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ScheduleDetails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ScheduleDetails']))
			$model->attributes=$_GET['ScheduleDetails'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ScheduleDetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ScheduleDetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ScheduleDetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='schedule-details-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionAutocompleteMatter($term)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(cod)', strtolower($_GET['term']), true);
        $criteria->order = 'cod';
        $criteria->limit = 30;
        $data = Matters::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->id,
                    'value' => $item->cod.' - '.$item->name,
                    'label' => $item->cod.' - '.$item->name,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }

    public function actionAutocompleteTeacher($term, $id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(name)', strtolower($_GET['term']), true);
        $criteria->compare('LOWER(matter_id)', strtolower($_GET['id']), 'and');
        $criteria->order = 'name';
        $criteria->limit = 30;
        $data = Users::model()->findAll($criteria);

        if (!empty($data))
        {
            $arr = array();
            foreach ($data as $item) {
                $arr[] = array(
                    'id' => $item->id,
                    'value' => $item->id,
                    'label' => $item->name.' '.$item->last_name.' - '.$item->matter->cod,
                );
            }
        }
        else
        {
            $arr = array();
            $arr[] = array(
                'id' => '',
                'value' => 'No se han encontrado resultados para su búsqueda',
                'label' => 'No se han encontrado resultados para su búsqueda',
            );
        }

        echo CJSON::encode($arr);
    }
}

<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="wide form"><!-- search-form Start-->

			<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'action'=>Yii::app()->createUrl(\$this->route),
				'method'=>'get',
			)); ?>\n"; ?>

			<?php foreach($this->tableSchema->columns as $column): ?>
			<?php
				$field=$this->generateInputField($this->modelClass,$column);
				if(strpos($field,'password')!==false)
					continue;
			?>
				<div>
					<?php echo "<?php echo \$form->label(\$model,Yii::t('app','{$column->name}')); ?>\n"; ?>
					<?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
				</div>
				<br>

			<?php endforeach; ?>
				<div class="buttons">
					<?php echo "<?php echo CHtml::submitButton(Yii::t('app', 'Search'), array('class'=>'btn btn-primary btn-lg')); ?>\n"; ?>
				</div>

			<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

			</div><!-- search-form End-->
		</div>
	</div>
</div>
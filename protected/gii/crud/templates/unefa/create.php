<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('app','$label')=>array('index'),
	Yii::t('app','Create'),
);\n";
?>
$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
$visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
$visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

$this->menu=array(
	array('label'=>Yii::t('app','List <?php echo $this->modelClass; ?>'), 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage <?php echo $this->modelClass; ?>'), 'url'=>array('admin')),
);
?>

<h1><?php echo "<?php echo"; ?> Yii::t('app','Create <?php echo $this->modelClass."'); ?>";?></h1>

<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>

<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('app','$label')=>array('index'),
	Yii::t('app', 'Manage'),
);\n";
?>

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
$visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
$visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

$this->menu=array(
	array('label'=>Yii::t('app', 'List <?php echo $this->modelClass; ?>'), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create <?php echo $this->modelClass; ?>'), 'url'=>array('create'), 'visible' => $visible),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo "<?php echo"?> Yii::t('app','Manage <?php echo $this->pluralize($this->class2name($this->modelClass))?>') ?></h1>

<p>
	<?php echo "<?php echo Yii::t('app', 'You may optionally enter a comparison operator').' (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) '.Yii::t('app', 'at the beginning of each of your search values to specify how the comparison should be done.'); ?>"; ?>
</p>

<!-- search-form Start-->
<?php echo "<?php echo CHtml::link(Yii::t('app', 'Advanced Search'),'#',array('class'=>'search-button')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div>
<!-- search-form End-->

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => TbHtml::GRID_TYPE_HOVER,
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'itemsCssClass'=>'table table-striped',
	'pager'=>array('htmlOptions'=>array('class'=>'pagination')),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('app','$label')=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$admin = (isset(Yii::app()->user->admin)) ? Yii::app()->user->admin : false ;
$coordinador = (isset(Yii::app()->user->coordinador)) ? Yii::app()->user->coordinador : false ;
$profesor = (isset(Yii::app()->user->profesor)) ? Yii::app()->user->profesor : false ;
$visible = (isset(Yii::app()->user->visible)) ? Yii::app()->user->visible : false ;
$visible2 = (isset(Yii::app()->user->visible2)) ? Yii::app()->user->visible2 : false ;
$visible3 = (isset(Yii::app()->user->visible3)) ? Yii::app()->user->visible3 : false ;

$this->menu=array(
	array('label'=>Yii::t('app', 'List <?php echo $this->modelClass; ?>'), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create <?php echo $this->modelClass; ?>'), 'url'=>array('create'), 'visible' => $visible),
	array('label'=>Yii::t('app', 'Update <?php echo $this->modelClass; ?>'), 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>Yii::t('app', 'Delete <?php echo $this->modelClass; ?>'), 'url'=>'#', 'visible' => $visible, 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>Yii::t('app','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('app', 'Manage <?php echo $this->modelClass; ?>'), 'url'=>array('admin')),
);
?>

<h1><?php echo "<?php echo"; ?> Yii::t('app','View <?php echo $this->modelClass."').' #'.\$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'type'=>'striped bordered condensed',
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>

/*
PGSQL Backup
Source Server Version: 9.6.2
Source Database: unefa
Date: 24/04/2017 01:14:59
*/
-- ----------------------------
--  Drop All Tables
-- ----------------------------
DROP TABLE IF EXISTS "public"."AuthItem" CASCADE;
DROP TABLE IF EXISTS "public"."AuthAssignment" ;
DROP TABLE IF EXISTS "public"."AuthItemChild" ;
DROP TABLE IF EXISTS "public"."matters" CASCADE;
DROP TABLE IF EXISTS "public"."careers" CASCADE;
DROP TABLE IF EXISTS "public"."pensums" CASCADE;
DROP TABLE IF EXISTS "public"."status" CASCADE;
DROP TABLE IF EXISTS "public"."users" CASCADE;
DROP TABLE IF EXISTS "public"."availability" ;
DROP TABLE IF EXISTS "public"."entities" CASCADE;
DROP TABLE IF EXISTS "public"."classrooms" CASCADE;
DROP TABLE IF EXISTS "public"."semesters" CASCADE;
DROP TABLE IF EXISTS "public"."schedules" CASCADE;
DROP TABLE IF EXISTS "public"."scheduleDetails" CASCADE;
DROP TABLE IF EXISTS "public"."studentslist" ;



-- ----------------------------
--  Table structure for "public"."entities"
-- ----------------------------
CREATE TABLE "public"."entities" (
 "id" int8 DEFAULT nextval('entities_id_seq'::regclass) NOT NULL,
 "name" varchar(255) COLLATE "default" NOT NULL,
 "description" varchar(255) COLLATE "default",
 PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Sequence definition for "public"."availability_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."availability_id_seq" CASCADE;
CREATE SEQUENCE "public"."availability_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."careers_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."careers_id_seq" CASCADE;
CREATE SEQUENCE "public"."careers_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;

SELECT setval('"public"."careers_id_seq"', 4, true);;

-- ----------------------------
--  Sequence definition for "public"."classrooms_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."classrooms_id_seq" CASCADE;
CREATE SEQUENCE "public"."classrooms_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."entities_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."entities_id_seq" CASCADE;
CREATE SEQUENCE "public"."entities_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

SELECT setval('"public"."entities_id_seq"', 23, true);;

-- ----------------------------
--  Sequence definition for "public"."matters_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."matters_id_seq" CASCADE;
CREATE SEQUENCE "public"."matters_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."pensums_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."pensums_id_seq" CASCADE;
CREATE SEQUENCE "public"."pensums_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

SELECT setval('"public"."pensums_id_seq"', 3, true);;

-- ----------------------------
--  Sequence definition for "public"."scheduleDetails_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."scheduleDetails_id_seq" CASCADE;
CREATE SEQUENCE "public"."scheduleDetails_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."schedules_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."schedules_id_seq" CASCADE;
CREATE SEQUENCE "public"."schedules_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;

SELECT setval('"public"."schedules_id_seq"', 7, true);;

-- ----------------------------
--  Sequence definition for "public"."semesters_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."semesters_id_seq" CASCADE;
CREATE SEQUENCE "public"."semesters_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."status_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."status_id_seq" CASCADE;
CREATE SEQUENCE "public"."status_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."studentslist_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."studentslist_id_seq" CASCADE;
CREATE SEQUENCE "public"."studentslist_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;;

-- ----------------------------
--  Sequence definition for "public"."users_id_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq" CASCADE;
CREATE SEQUENCE "public"."users_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;

SELECT setval('"public"."users_id_seq"', 2, true);;

-- ----------------------------
--  Table structure for "public"."AuthItem"
-- ----------------------------
CREATE TABLE "public"."AuthItem" (
 "name" varchar(255) COLLATE "default" NOT NULL,
 "type" varchar(255) COLLATE "default" NOT NULL,
 "description" text COLLATE "default",
 "bizrule" text COLLATE "default",
 "data" text COLLATE "default",
 PRIMARY KEY ("name")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."AuthAssignment"
-- ----------------------------
CREATE TABLE "public"."AuthAssignment" (
"itemname" varchar(255) COLLATE "default" NOT NULL,
"userid" int8 NOT NULL,
"bizrule" text COLLATE "default",
"data" text COLLATE "default",
PRIMARY KEY ("userid"),
FOREIGN KEY ("itemname") REFERENCES "public"."AuthItem" ("name") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."AuthItemChild"
-- ----------------------------
CREATE TABLE "public"."AuthItemChild" (
"parent" varchar(255) COLLATE "default" NOT NULL,
"child" varchar(255) COLLATE "default" NOT NULL,
PRIMARY KEY ("parent", "child"),
FOREIGN KEY ("child") REFERENCES "public"."AuthItem" ("name") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("parent") REFERENCES "public"."AuthItem" ("name") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."matters"
-- ----------------------------
CREATE TABLE "public"."matters" (
 "id" int8 DEFAULT nextval('matters_id_seq'::regclass) NOT NULL,
 "cod" varchar(255) COLLATE "default" NOT NULL,
 "name" varchar(255) COLLATE "default" NOT NULL,
 "priority1" varchar(255) COLLATE "default" NOT NULL,
 "priority2" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
 "unitsCredit" int4 NOT NULL,
 "hoursLab" int4 DEFAULT 0,
 "hoursTheory" int4 NOT NULL,
 "hoursPractice" int4 DEFAULT 0,
 PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."careers"
-- ----------------------------
CREATE TABLE "public"."careers" (
 "id" int8 DEFAULT nextval('careers_id_seq'::regclass) NOT NULL,
 "name" varchar(255) COLLATE "default" NOT NULL,
 "description" varchar(255) COLLATE "default" NOT NULL,
 PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."pensums"
-- ----------------------------
CREATE TABLE "public"."pensums" (
 "id" int8 DEFAULT nextval('pensums_id_seq'::regclass) NOT NULL,
 "year" int4 NOT NULL,
 "turn" int8 NOT NULL,
 "career_id" int8 NOT NULL,
 PRIMARY KEY ("id"),
 FOREIGN KEY ("career_id") REFERENCES "public"."careers" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("turn") REFERENCES "public"."entities" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."status"
-- ----------------------------
CREATE TABLE "public"."status" (
 "id" int8 DEFAULT nextval('status_id_seq'::regclass) NOT NULL,
 "name" varchar(255) COLLATE "default" NOT NULL,
 PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."users"
-- ----------------------------
CREATE TABLE "public"."users" (
 "id" int8 DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
 "ci" int4,
 "name" varchar(255) COLLATE "default" NOT NULL,
 "last_name" varchar(255) COLLATE "default" NOT NULL,
 "birthday" date NOT NULL,
 "address" varchar(1500) COLLATE "default" NOT NULL,
 "phone" varchar(15) COLLATE "default" NOT NULL,
 "type" varchar(15) COLLATE "default" NOT NULL,
 "matter_id" int8,
 "pensum_id" int8,
 "username" varchar(255) COLLATE "default" NOT NULL,
 "email" varchar(255) COLLATE "default" NOT NULL,
 "auth_key" varchar(32) COLLATE "default" NOT NULL,
 "accessToken" varchar(32) COLLATE "default" NOT NULL,
 "password_hash" varchar(255) COLLATE "default" NOT NULL,
 "password_reset_token" varchar(255) COLLATE "default",
 "status_id" int8 DEFAULT 3 NOT NULL,
 "created_at" date NOT NULL,
 "updated_at" date NOT NULL,
 PRIMARY KEY ("id"),
 FOREIGN KEY ("pensum_id") REFERENCES "public"."pensums" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter_id") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("status_id") REFERENCES "public"."status" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 UNIQUE ("username"),
 UNIQUE ("email"),
 UNIQUE ("password_reset_token")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."availability"
-- ----------------------------
CREATE TABLE "public"."availability" (
"id" int8 DEFAULT nextval('availability_id_seq'::regclass) NOT NULL,
"days" int8 NOT NULL,
"hour_start" int8,
"hour_end" int8,
"matter_id" int8,
"user_id" int8,
PRIMARY KEY ("id"),
FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter_id") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("days") REFERENCES "public"."entities" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("hour_start") REFERENCES "public"."entities" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("hour_end") REFERENCES "public"."entities" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."classrooms"
-- ----------------------------
CREATE TABLE "public"."classrooms" (
"id" int8 DEFAULT nextval('classrooms_id_seq'::regclass) NOT NULL,
"cod" varchar(255) COLLATE "default" NOT NULL,
PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."semesters"
-- ----------------------------
CREATE TABLE "public"."semesters" (
 "id" int8 DEFAULT nextval('semesters_id_seq'::regclass) NOT NULL,
 "name" varchar(255) COLLATE "default" NOT NULL,
 "career_id" int8 NOT NULL,
 "pensum_id" int8 NOT NULL,
 "matter" int8 NOT NULL,
 "matter1" int8 DEFAULT 1,
 "matter2" int8 DEFAULT 1,
 "matter3" int8 DEFAULT 1,
 "matter4" int8 DEFAULT 1,
 "matter5" int8 DEFAULT 1,
 "matter6" int8 DEFAULT 1,
 "matter7" int8 DEFAULT 1,
 "matter8" int8 DEFAULT 1,
 "matter9" int8 DEFAULT 1,
 PRIMARY KEY ("id"),
 FOREIGN KEY ("matter6") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter9") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter5") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter4") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter3") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter2") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter1") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("pensum_id") REFERENCES "public"."pensums" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("career_id") REFERENCES "public"."careers" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter8") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("matter7") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."schedules"
-- ----------------------------
CREATE TABLE "public"."schedules" (
 "id" int8 DEFAULT nextval('schedules_id_seq'::regclass) NOT NULL,
 "classroom_id" int8 NOT NULL,
 "semester_id" int8 NOT NULL,
 PRIMARY KEY ("id"),
 FOREIGN KEY ("semester_id") REFERENCES "public"."semesters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY ("classroom_id") REFERENCES "public"."classrooms" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."scheduleDetails"
-- ----------------------------
CREATE TABLE "public"."scheduleDetails" (
"id" int8 DEFAULT nextval('"scheduleDetails_id_seq"'::regclass) NOT NULL,
"schedule_id" int8 NOT NULL,
"day" int2 NOT NULL,
"block1" int8 NOT NULL,
"block2" int8 NOT NULL,
"block3" int8 NOT NULL,
"block4" int8 NOT NULL,
"block5" int8 NOT NULL,
"block6" int8 NOT NULL,
"block7" int8 NOT NULL,
"block8" int8 NOT NULL,
"block9" int8 NOT NULL,
"block10" int8 NOT NULL,
"block11" int8 NOT NULL,
"block12" int8 NOT NULL,
"block13" int8 NOT NULL,
"user1_id" int8 NOT NULL,
"user2_id" int8 NOT NULL,
"user3_id" int8 NOT NULL,
"user4_id" int8 NOT NULL,
"user5_id" int8 NOT NULL,
"user6_id" int8 NOT NULL,
"user7_id" int8 NOT NULL,
"user8_id" int8 NOT NULL,
"user9_id" int8 NOT NULL,
"user10_id" int8 NOT NULL,
"user11_id" int8 NOT NULL,
"user12_id" int8 NOT NULL,
"user13_id" int8 NOT NULL,
PRIMARY KEY ("id"),
FOREIGN KEY ("schedule_id") REFERENCES "public"."schedules" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block3") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block4") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block5") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block6") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block7") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block8") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block9") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block10") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block11") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block12") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block13") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user1_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user2_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user3_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user4_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user5_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user6_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user7_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user8_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user9_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user10_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user11_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user12_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("user13_id") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("day") REFERENCES "public"."entities" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block1") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ("block2") REFERENCES "public"."matters" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."studentslist"
-- ----------------------------
CREATE TABLE "public"."studentslist" (
"id" int8 DEFAULT nextval('studentslist_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"classroom_id" int4 NOT NULL,
PRIMARY KEY ("id"),
FOREIGN KEY ("classroom_id") REFERENCES "public"."classrooms" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  View definition for "public"."teachersMatters"
-- ----------------------------
DROP VIEW IF EXISTS "public"."teachersMatters";
CREATE VIEW "public"."teachersMatters" AS 
 SELECT u.id AS "ID",
    u.name AS "Name",
    u.last_name AS "Last_name",
    m.name AS "Matters",
    m.cod AS "Cod"
   FROM (users u
     JOIN matters m ON ((u.matter_id = m.id)));;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO "public"."AuthItem" VALUES ('Admin','2','Administrador de la Aplicación',NULL,'N;'); INSERT INTO "public"."AuthItem" VALUES ('Coordinador','2','Coordinador de Carrera',NULL,'N;'); INSERT INTO "public"."AuthItem" VALUES ('Estudiante','2','Estudiante de la UNEFA',NULL,'N;'); INSERT INTO "public"."AuthItem" VALUES ('Profesor','2','Profesor de la UNEFA',NULL,'N;');
INSERT INTO "public"."AuthAssignment" VALUES ('Admin','1',NULL,'N;'); 
INSERT INTO "public"."matters" VALUES ('1','','N/A','',NULL,'0',NULL,'0',NULL); INSERT INTO "public"."matters" VALUES ('2','ADG-25123','Hombre, Sociedad, Ciencia y Tecnologia','N/A','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('3','ADG-25132','Educación Ambiental','N/A','','2','0','2','1'); INSERT INTO "public"."matters" VALUES ('4','IDM-24113','Ingles I','N/A','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('5','MAT-21212','Dibujo','N/A','','2','0','1','3'); INSERT INTO "public"."matters" VALUES ('6','MAT-21215','Matemática I','N/A','','5','0','4','2'); INSERT INTO "public"."matters" VALUES ('7','MAT-21524','Geometría Analítica','N/A','','4','0','3','2'); INSERT INTO "public"."matters" VALUES ('8','ADG-25131','Seminario I','N/A','','1','0','1','0'); INSERT INTO "public"."matters" VALUES ('9','DIN-21113','Defensa Integral de la Nación I','N/A','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('10','IDM-24123','Ingles II','IDM-24113','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('11','QUF-22014','Quimica General','N/A','','4','3','2','2'); INSERT INTO "public"."matters" VALUES ('12','QUF-23015','Física I','MAT-21215','MAT-21524','5','2','4','2'); INSERT INTO "public"."matters" VALUES ('13','MAT-21225','Matemática II','MAT-21215','MAT-21524','5','0','4','2'); INSERT INTO "public"."matters" VALUES ('14','MAT-21114','Álgebra Lineal','MAT-21215','MAT-21524','4','0','2','4'); INSERT INTO "public"."matters" VALUES ('15','ADG-25141','Seminario II','ADG-25131','','1','0','1','0'); INSERT INTO "public"."matters" VALUES ('16','DIN-21123','Defensa Integral de la Nación II','DIN-21113','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('17','QUF-23025','Física II','QUF-23015','MAT-21225','5','2','4','2'); INSERT INTO "public"."matters" VALUES ('18','MAT-21235','Matemática III','MAT-21225','','5','0','4','2'); INSERT INTO "public"."matters" VALUES ('19','MAT-21414','Probabilidad y Estadística','MAT-21225','','4','0','2','4'); INSERT INTO "public"."matters" VALUES ('20','SYC-22113','Programación','MAT-21114','','3','3','2','0'); INSERT INTO "public"."matters" VALUES ('21','AGG-22313','Sistemas Administrativos','N/A','','4','0','3','3'); INSERT INTO "public"."matters" VALUES ('22','DIN-21133','Defensa Integral de la Nación III','DIN-21123','','3','0','2','2'); INSERT INTO "public"."matters" VALUES ('23','SYC-32114','Teoría de los Sistemas','N/A','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('24','MAT-31714','Cálculo Numérico','MAT-21235','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('25','MAT-31214','Lógica Matemática','MAT-21114','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('26','SYC-32225','Lenguaje de Programación I','SYC-22113','','5','3','4',NULL); INSERT INTO "public"."matters" VALUES ('27','SYC-32414','Procesamiento de Datos','SYC-22113','','4','3','3',NULL); INSERT INTO "public"."matters" VALUES ('28','DIN-31143','Defensa Integral de la Nación IV','DIN-21133','','3',NULL,'2','2'); INSERT INTO "public"."matters" VALUES ('29','MAT-31114','Teoría de Grafos','MAT-31213','MAT-21413','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('30','SYC-32235','Lenguaje de Programación II','SYC-32225','','5','3','4',NULL); INSERT INTO "public"."matters" VALUES ('31','MAT-30925','Investigación de Operaciones','MAT-31714','','5',NULL,'4','3'); INSERT INTO "public"."matters" VALUES ('32','ELN-30514','Circuitos Logícos','MAT-31214','','4',NULL,'3','2'); INSERT INTO "public"."matters" VALUES ('33','SYC-32514','Análisis de Sistemas','SYC-32114','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('34','syc-32614','Base de Datos','SYC-32114','','4','3','3',NULL); INSERT INTO "public"."matters" VALUES ('35','DIN-31153','Defensa Integral de la Nación V','DIN-31143','','3',NULL,'2','2'); INSERT INTO "public"."matters" VALUES ('36','MAT-30935','Optimización No Linea ','MAT-30925','','5',NULL,'4','3'); INSERT INTO "public"."matters" VALUES ('37','SYC-32245','Lenguaje de Programación III','SYC-32235','','5','3','4',NULL); INSERT INTO "public"."matters" VALUES ('38','MAT-31414','Procesos Estocastícos','MAT-30925','MAT-31114','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('39','SYC-30525','Arquitectura del Computador','ELN-30514','','5',NULL,'4','3'); INSERT INTO "public"."matters" VALUES ('40','SYC-32524','Diseño de Sistemas','SYC-32514','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('41','SYC-30834','Sistemas Operativos','SYC-30525','','4','3','3',NULL); INSERT INTO "public"."matters" VALUES ('42','DIN-31163','Defensa Integral de la Nación VI','DIN-31153','','3',NULL,'2','2'); INSERT INTO "public"."matters" VALUES ('43','SYC-32714','Implantación de Sistemas','SYC-32524','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('44','ADG-30214','Metodología de la Investigación','N/A','','4',NULL,'3','2'); INSERT INTO "public"."matters" VALUES ('45','MAT-30945','Simulación y Modelos','MAT-30935','MAT-31414','5','3','4',NULL); INSERT INTO "public"."matters" VALUES ('46','SYC-31644','Redes','SYC-30834','','4','3','3',NULL); INSERT INTO "public"."matters" VALUES ('47','ADG-30224','Gerencia de la Informatica','N/A','','4',NULL,'3','2'); INSERT INTO "public"."matters" VALUES ('48','ET-7','Electiva Tecnica','N/A','','3',NULL,'3',NULL); INSERT INTO "public"."matters" VALUES ('49','ENT-7','Electiva No Tecnica','N/A','','3',NULL,'3',NULL); INSERT INTO "public"."matters" VALUES ('50','DIN-31173','Defensa Integral de la Nación VII','DIN-31163','','3',NULL,'2','2'); INSERT INTO "public"."matters" VALUES ('51','MAT-31314','Teoría de Decisiones','MAT-30945','','4',NULL,'3','2'); INSERT INTO "public"."matters" VALUES ('52','SYC-32814','Auditoria de SIstemas','SYC-32714','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('53','CJU-37314','Marco Legal Para el Ejercicio de la Ingenieria','N/A','','4',NULL,'4',NULL); INSERT INTO "public"."matters" VALUES ('54','TTC-31154','Teleprocesos','SYC-31644','','4',NULL,'3','3'); INSERT INTO "public"."matters" VALUES ('55','ET-8','Electiva Tecnica','N/A','','3',NULL,'3',NULL); INSERT INTO "public"."matters" VALUES ('56','ENT-8','Electiva No Tecnica','N/A','','3',NULL,'3',NULL); INSERT INTO "public"."matters" VALUES ('57','DIN-31183','Defensa Integral de la Nación VIII','DIN-31173','','3',NULL,'2','2'); INSERT INTO "public"."matters" VALUES ('58','TGR-30010','Trabajo Especial de Grado','N/A','','10',NULL,'0',NULL); INSERT INTO "public"."matters" VALUES ('59','AGL-30214','Sistemas de Produccion','AGG-22313','','4',NULL,'3','3');
INSERT INTO "public"."careers" VALUES ('1','N\A','No Aplica'); INSERT INTO "public"."careers" VALUES ('2','Ing. de Sistemas','Ingenieria de Sistemas'); INSERT INTO "public"."careers" VALUES ('3','Ing. de Telecomunicaciones','Ingenieria de Telecomunicaciones'); INSERT INTO "public"."careers" VALUES ('4','Ing. Telecomunicaciones','Ingenieria en Telecomunicaciones');
INSERT INTO "public"."status" VALUES ('1','Activo'); INSERT INTO "public"."status" VALUES ('2','Inactivo'); INSERT INTO "public"."status" VALUES ('3','Primer Ingreso');
INSERT INTO "public"."users" VALUES ('1','18277864','Jose','Guerrero','2016-05-20','por aki','04263412354','Admin',NULL,NULL,'morogasper','joseaguerreroh@gmail.com','iOydYg-SmYPu2PI6BULj7Uj52Ab7sCXZ','3duqHWz82IQxIPAFXK-M-H3KMz32x7sR','e10adc3949ba59abbe56e057f20f883e',NULL,'1','2016-05-20','2016-05-20'); 
INSERT INTO "public"."classrooms" VALUES ('1','1001'); INSERT INTO "public"."classrooms" VALUES ('2','1002'); INSERT INTO "public"."classrooms" VALUES ('3','1003'); INSERT INTO "public"."classrooms" VALUES ('4','1004'); INSERT INTO "public"."classrooms" VALUES ('5','1005'); INSERT INTO "public"."classrooms" VALUES ('6','1006'); INSERT INTO "public"."classrooms" VALUES ('7','1007'); INSERT INTO "public"."classrooms" VALUES ('8','1008'); INSERT INTO "public"."classrooms" VALUES ('9','1009'); INSERT INTO "public"."classrooms" VALUES ('10','1010'); INSERT INTO "public"."classrooms" VALUES ('11','1011'); INSERT INTO "public"."classrooms" VALUES ('12','1012'); INSERT INTO "public"."classrooms" VALUES ('13','1013'); INSERT INTO "public"."classrooms" VALUES ('14','1014'); INSERT INTO "public"."classrooms" VALUES ('15','1015'); INSERT INTO "public"."classrooms" VALUES ('16','1016'); INSERT INTO "public"."classrooms" VALUES ('17','1017'); INSERT INTO "public"."classrooms" VALUES ('18','1018'); INSERT INTO "public"."classrooms" VALUES ('19','1019');
INSERT INTO "public"."entities" VALUES ('1','Lunes','Days'); INSERT INTO "public"."entities" VALUES ('2','Martes','Days'); INSERT INTO "public"."entities" VALUES ('3','Miercoles','Days'); INSERT INTO "public"."entities" VALUES ('4','Jueves','Days'); INSERT INTO "public"."entities" VALUES ('5','Viernes','Days'); INSERT INTO "public"."entities" VALUES ('6','Sabado','Days'); INSERT INTO "public"."entities" VALUES ('7','Domingo','Days'); INSERT INTO "public"."entities" VALUES ('8','7:30','block'); INSERT INTO "public"."entities" VALUES ('9','8:15','block'); INSERT INTO "public"."entities" VALUES ('10','9:00','block'); INSERT INTO "public"."entities" VALUES ('11','9:45','block'); INSERT INTO "public"."entities" VALUES ('12','10:30','block'); INSERT INTO "public"."entities" VALUES ('13','11:15','block'); INSERT INTO "public"."entities" VALUES ('14','12:00','block'); INSERT INTO "public"."entities" VALUES ('15','1:00','block'); INSERT INTO "public"."entities" VALUES ('16','1:45','block'); INSERT INTO "public"."entities" VALUES ('17','2:30','block'); INSERT INTO "public"."entities" VALUES ('18','3:15','block'); INSERT INTO "public"."entities" VALUES ('19','4:00','block'); INSERT INTO "public"."entities" VALUES ('20','4:45','block'); INSERT INTO "public"."entities" VALUES ('21','5:30','block'); INSERT INTO "public"."entities" VALUES ('22','Diurno','Turn'); INSERT INTO "public"."entities" VALUES ('23','Nocturno','Turn');
INSERT INTO "public"."pensums" VALUES ('1','1800','22','1'); INSERT INTO "public"."pensums" VALUES ('2','2009','22','1'); INSERT INTO "public"."pensums" VALUES ('3','2010','22','2');
INSERT INTO "public"."semesters" VALUES ('1','1° Semestre','1','1','3','2','4','5','6','7','8','9','1','1'); INSERT INTO "public"."semesters" VALUES ('2','2° Semestre','1','1','10','11','12','13','14','15','16','1','1','1'); INSERT INTO "public"."semesters" VALUES ('3','3° Semestre','1','1','17','18','19','20','21','37','1','1','1','1'); INSERT INTO "public"."semesters" VALUES ('4','4° Semestre','1','1','23','24','25','26','27','59','28','1','1','1'); INSERT INTO "public"."semesters" VALUES ('5','5° Semestre','1','1','29','30','31','32','33','34','35','1','1','1'); INSERT INTO "public"."semesters" VALUES ('6','6° Semestre','1','1','36','37','38','39','40','41','42','1','1','1'); INSERT INTO "public"."semesters" VALUES ('7','7° Semestre','1','1','43','44','45','46','47','48','49','50','1','1'); INSERT INTO "public"."semesters" VALUES ('8','8° Semestre','1','1','51','52','53','54','55','56','57','1','1','1'); INSERT INTO "public"."semesters" VALUES ('9','Pasantias','1','1','58','1','1','1','1','1','1','1','1','1');
INSERT INTO "public"."schedules" VALUES ('1','1','1'); INSERT INTO "public"."schedules" VALUES ('2','1','1'); INSERT INTO "public"."schedules" VALUES ('3','1','1'); INSERT INTO "public"."schedules" VALUES ('4','3','4'); INSERT INTO "public"."schedules" VALUES ('5','5','6'); INSERT INTO "public"."schedules" VALUES ('6','5','4'); INSERT INTO "public"."schedules" VALUES ('7','4','1');

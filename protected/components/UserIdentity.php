<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
	class UserIdentity extends CUserIdentity
	{
		// Definiendo Variables Privadas
		private $_id;
		/**
		 * Authenticates a user.
		 * The example implementation makes sure if the username and password
		 * are both 'demo'.
		 * In practical applications, this should be changed to authenticate
		 * against some persistent user identity storage (e.g. database).
		 * @return boolean whether authentication succeeds.
		 */
		public function authenticate()
		{
			//Consultar a la DB si existe registro igual al username ingresado en el form
			$user=Users::model()->find("LOWER(username)=?", array(strtolower($this->username)));

			if($user===NULL) //Devolver error si no existe Username
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			else {
				if(md5($this->password)!==$user->password_hash) //Devolver error si no coincide Password
					$this->errorCode=self::ERROR_PASSWORD_INVALID;
				else {
					if ($user->status_id==2)
						$this->errorCode=self::ERROR_USERNAME_NOT_ACTIVE; //Devolver error si el status en Inactivo
					else
					{
						foreach (Yii::app()->authManager->getAuthAssignments($user->id) as $data)
						{
							#$enabled=Yii::app()->authManager->checkAccess($data->name, $model->id);
							if ($user->id==$data->userid and $data->itemName=='Admin')
							{
								$perfil1 = $data->itemName;
							}
							else
							{
								if ($user->id==$data->userid and $data->itemName=='Coordinador')
								{
									$perfil2 = $data->itemName;
								}
								else
								{
									if ($user->id==$data->userid and $data->itemName=='Profesor')
									{
										$perfil3 = $data->itemName;
									}
									else
									{
										if($user->id==$data->userid and $data->itemName=='Estudiante')
										{
											$perfil4 = $data->itemName;
										}
									}
								}
							}
						}

						//Asignando valores a las Variables para el control de Usuarios
						$admin = (isset($perfil1) and $perfil1=='Admin') ? true : false ;
						$coordinador = (isset($perfil2) and $perfil2=='Coordinador') ? true : false ;
						$profesor = (isset($perfil3) and $perfil3=='Profesor') ? true : false ;
						$estudiante = (isset($perfil4) and $perfil4=='Estudiante') ? true : false ;
						$visible = ($admin or $coordinador) ? true : false ;
						$visible2 = ($visible or $profesor) ? true : false ;
						$visible3 = ($visible2 or $estudiante) ? true : false ;
						$name = $user->name.' '.$user->last_name;
						$username = $user->username;

						//Setear variables de session para tener acceso desde
						//la aplicación usando Yii::app()->user->id
						$this->_id = $user->id;
						$this->setState("id", $user->id);
						$this->setState("username", $username);
						$this->setState("email", $user->email);
						$this->setState("name", $name);
						$this->setState("admin", $admin);
						$this->setState("coordinador", $coordinador);
						$this->setState("profesor", $profesor);
						$this->setState("estudiante", $estudiante);
						$this->setState("status", $user->status_id);
						$this->setState("visible", $visible);
						$this->setState("visible2", $visible2);
						$this->setState("visible3", $visible3);
						$this->errorCode=self::ERROR_NONE;
					}
				}
			}
			return !$this->errorCode;
		}

		public function getId()
		{
			return $this->_id;
		}
	}